import { test, expect } from '@playwright/test';


test.describe('Logout component', () => {

    test('logsout user', async ({ page }) => {
        await page.goto('http://localhost:3000/login');
        await page.waitForTimeout(500);
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByText('Logout').click();
        await expect(page.getByRole('button', { name: 'Login' })).toBeVisible();
        await expect(page.getByRole('button', { name: 'Create an Account' })).toBeVisible();
    });
});