import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
  await page.goto('http://localhost:3000');
  await page.waitForTimeout(500);
});


test.describe('ViewEvents component', () => {

  test('renders landing page', async ({ page }) => {
    await expect(page.getByText('Welcome to EventPoint')).toBeVisible();
  });

  test('filters events by search input', async ({ page }) => {
    await page.getByPlaceholder('Search Events...').click();
    await page.waitForTimeout(1000);
    await page.keyboard.press('a');
    await page.locator('.row > button:nth-child(2)').first().click();
    await expect(page.getByRole('dialog')).toBeVisible();
    await page.getByRole('button', { name: 'Close' }).click();
  });

  test('filters events by venue dropdown', async ({ page }) => {
    await page.locator('#dateDropdown').selectOption('3Arena');
    await page.getByRole('button', { name: 'View Details' }).first().click();
    await expect(page.getByRole('dialog')).toBeVisible();
    await page.getByRole('button', { name: 'Close' }).click();
  });

  test('filters events by date dropdown', async ({ page }) => {
    await page.locator('#dateVenue').selectOption('2024-03-16');
    await page.getByRole('button', { name: 'View Details' }).click();
    await expect(page.getByRole('dialog')).toBeVisible();
    await page.getByRole('button', { name: 'Close' }).click();
  });

  test('shows no events when no events match the search filters', async ({ page }) => {
    await page.getByPlaceholder('Search Events...').click();
    await page.getByPlaceholder('Search Events...').fill('no events');
    await expect(page.getByText('No Results Found')).toBeVisible();
  });

});

test.describe('EventInformation component', () => {
  test('renders event information modal when events are shown', async ({ page }) => {
    await page.getByRole('button', { name: 'View Details' }).first().click();
    await expect(page.getByRole('dialog')).toBeVisible();
  });
});