import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
    await page.waitForTimeout(250);
  });

test.describe('SendFriendRequest component', () => {

    let context1;
    let context2;
    let page1;
    let page2;

    test.beforeAll(async ({ browser }) => {
        context1 = await browser.newContext();
        context2 = await browser.newContext();
    });

    test.afterAll(async () => {
        await Promise.all([
            context1.close(),
            context2.close()
        ]);
    });

    test('user1 send friend request', async ({ context }) => {
        page1 = await context1.newPage();
        await page1.goto('http://localhost:3000/login');
        await page1.getByPlaceholder('Enter email').click();
        await page1.getByPlaceholder('Enter email').fill('testing@email.com');
        await page1.getByPlaceholder('Enter password').click();
        await page1.getByPlaceholder('Enter password').fill('Password1!');
        await page1.locator('form').getByRole('button', { name: 'Login' }).click();
        await page1.waitForTimeout(1000);
        await page1.getByRole('button', { name: 'Account' }).click();
        await page1.getByLabel('Account').getByRole('button', { name: 'Account' }).click();
        await expect(page1.getByText('Username: testing')).toBeVisible();

        await page1.waitForTimeout(1000);
        await page1.getByRole('button', { name: 'Users' }).click();
        await page1.getByRole('button', { name: 'View Profile' }).nth(1).click();
        page1.on('dialog', async dialog => {
            await dialog.dismiss();
          });
        await page1.getByRole('button', { name: 'Send Friend Request' }).click();
    });

    test('user2 reject friend request', async ({ context }) => {
        page2 = await context2.newPage();
        await page2.goto('http://localhost:3000/login');
        await page2.getByPlaceholder('Enter email').click();
        await page2.getByPlaceholder('Enter email').fill('testing2@email.com');
        await page2.getByPlaceholder('Enter password').click();
        await page2.getByPlaceholder('Enter password').fill('Password1!');
        await page2.locator('form').getByRole('button', { name: 'Login' }).click();
        await page2.waitForTimeout(1000);
        await page2.getByRole('button', { name: 'Account' }).click();
        await page2.getByLabel('Account').getByRole('button', { name: 'Account' }).click();
        await expect(page2.getByText('Username: testing2')).toBeVisible();

        await page2.waitForTimeout(1500);
        await page2.getByRole('button', { name: 'Notifications' }).click();
        await expect(page2.getByText('Friend request from: testing')).toBeVisible();
        await page2.getByRole('button', { name: 'View' }).click();
        await page2.getByRole('button', { name: 'Reject' }).click();
    });

    test('user2 friend list is not updated', async ({ context }) => {
        await page2.waitForTimeout(1000);
        await page2.getByRole('button', { name: 'Account' }).click();
        await page2.waitForTimeout(1000);
        await page2.getByRole('button', { name: 'Profile' }).click();
        await expect(page2.getByText('Number of Friends: No friends found')).toBeVisible();
    })
    // test('user1 clears notification of friend request rejection', async ({ context }) => {
    //     await page1.waitForTimeout(1000);
    //     await page1.reload();
    //     await page1.waitForTimeout(1000);
    //     await page1.getByRole('button', { name: 'Notifications' }).click();
    //     await page1.waitForTimeout(1500);
    //     await expect(page1.getByText('testing2 has rejected your')).toBeVisible();
    //     await page1.getByRole('button', { name: 'X' }).click();
    // })

    test('user1 friend list is not updated', async ({ context }) => {
        await page1.waitForTimeout(1000);
        await page1.getByRole('button', { name: 'Account' }).click();
        await page1.waitForTimeout(1000);
        await page1.getByRole('button', { name: 'Profile' }).click();
        await expect(page1.getByText('Number of Friends: 1')).toBeVisible();
    })

    test('user1 send friend another request', async ({ context }) => {
        await page1.waitForTimeout(1000);

        await page1.getByRole('button', { name: 'Users' }).click();
        await page1.getByRole('button', { name: 'View Profile' }).nth(1).click();
        await page1.getByRole('button', { name: 'Send Friend Request' }).click();
    });

    test('user2 accept friend request', async ({ context }) => {
        await page2.waitForTimeout(1000);

        await page2.getByRole('button', { name: 'Notifications' }).click();
        await expect(page2.getByText('Friend request from: testing')).toBeVisible();
        await page2.getByRole('button', { name: 'View' }).click();
        await page2.getByRole('button', { name: 'Accept' }).click();
    });

    test('user2 friend list is updated', async ({ context }) => {
        await page2.waitForTimeout(1000);
        await page2.getByRole('button', { name: 'Account' }).click();
        await page2.waitForTimeout(1000);
        await page2.getByRole('button', { name: 'Profile' }).click();
        await expect(page2.getByText('Number of Friends: 1')).toBeVisible();
        await expect(page2.getByText('testing', { exact: true })).toBeVisible();
        await page2.getByText('testing', { exact: true }).click();
        await expect(page2.getByRole('heading', { name: 'testing' })).toBeVisible();
    })

    // test('user1 clears notification of friend request acceptance', async ({ context }) => {
    //     await page1.reload();
    //     await page1.getByRole('button', { name: 'Notifications' }).click();
    //     await page1.waitForTimeout(1500);
    //     await expect(page1.getByText('testing2 has accepted your')).toBeVisible();
    //     await page1.getByRole('button', { name: 'X' }).click();
    // })

    test('user1 friend list is updated', async ({ context }) => {
        await page1.waitForTimeout(1000);
        await page1.getByRole('button', { name: 'Account' }).click();
        await page1.waitForTimeout(1000);
        await page1.getByRole('button', { name: 'Profile' }).click();
        await expect(page1.getByText('Number of Friends: 2')).toBeVisible();
        await expect(page1.getByText('testing2', { exact: true })).toBeVisible();
        await page1.getByText('testing2', { exact: true }).click();
        await expect(page1.getByRole('heading', { name: 'testing2' })).toBeVisible();
    })

});


test.describe('RemoveFriend component', () => {

    let context1;
    let context2;
    let page1;
    let page2;

    test.beforeAll(async ({ browser }) => {
        context1 = await browser.newContext();
        context2 = await browser.newContext();
    });

    test.afterAll(async () => {
        await Promise.all([
            context1.close(),
            context2.close()
        ]);
    });

    test('user1 remove user2', async ({ context }) => {
        page1 = await context1.newPage();
        await page1.goto('http://localhost:3000/login');
        await page1.getByPlaceholder('Enter email').click();
        await page1.getByPlaceholder('Enter email').fill('testing@email.com');
        await page1.getByPlaceholder('Enter password').click();
        await page1.getByPlaceholder('Enter password').fill('Password1!');
        await page1.locator('form').getByRole('button', { name: 'Login' }).click();
        await page1.waitForTimeout(1000);
        await page1.getByRole('button', { name: 'Account' }).click();
        await page1.getByLabel('Account').getByRole('button', { name: 'Account' }).click();
        await expect(page1.getByText('Username: testing')).toBeVisible();

        await page1.waitForTimeout(1000);
        await page1.getByRole('button', { name: 'Account' }).click();
        await page1.getByRole('button', { name: 'Profile' }).click();
        await expect(page1.getByText('testing2')).toBeVisible();
        await page1.getByText('testing2').click();
        await expect(page1.getByRole('heading', { name: 'testing2' })).toBeVisible();
        page1.on('dialog', async dialog => {
            await dialog.accept();
          });
        await page1.getByRole('button', { name: 'Remove Friend' }).click();
        await expect(page1.getByRole('button', { name: 'Send Friend Request'})).toBeVisible();
    });

    test('user1 friend list updated after remove', async ({ context }) => {
        await page1.waitForTimeout(1000);
        await page1.getByRole('button', { name: 'Account' }).click();
        await page1.getByRole('button', { name: 'Profile' }).click();
        await expect(page1.getByText('Number of Friends: 1')).toBeVisible();
    })

    test('user2 friend list updated after remove', async ({ context }) => {
        page2 = await context2.newPage();
        await page2.goto('http://localhost:3000/login');
        await page2.getByPlaceholder('Enter email').click();
        await page2.getByPlaceholder('Enter email').fill('testing2@email.com');
        await page2.getByPlaceholder('Enter password').click();
        await page2.getByPlaceholder('Enter password').fill('Password1!');
        await page2.locator('form').getByRole('button', { name: 'Login' }).click();
        await page2.waitForTimeout(1000);
        await page2.getByRole('button', { name: 'Account' }).click();
        await page2.getByLabel('Account').getByRole('button', { name: 'Account' }).click();
        await expect(page2.getByText('Username: testing2')).toBeVisible();

        await page2.waitForTimeout(1000);
        await page2.getByRole('button', { name: 'Account' }).click();
        await page2.getByRole('button', { name: 'Profile' }).click();
        await expect(page2.getByText('Number of Friends: No friends found')).toBeVisible();
    });

});