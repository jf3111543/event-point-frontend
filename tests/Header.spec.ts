import { test, expect } from '@playwright/test';

test.beforeEach(async ({page}) => {
    await page.goto('http://localhost:3000');
    await page.waitForTimeout(500);
  });

test.describe('Logout component', () => {

    test('header for logged out user', async ({ page }) => {
        await expect(page.getByRole('button', { name: 'Login' })).toBeVisible();
        await expect(page.getByRole('button', { name: 'Create an Account' })).toBeVisible();
    });
    test('header for logged in user', async ({ page }) => {
        await page.goto('http://localhost:3000/login');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
        await expect(page.getByRole('button', { name: 'Account' })).toBeVisible();
        await expect(page.getByRole('button', { name: 'Users' })).toBeVisible();
        await expect(page.getByRole('button', { name: 'Notifications' })).toBeVisible();
        await page.getByRole('button', { name: 'Account' }).click();
        await expect(page.getByLabel('Account').getByRole('button', { name: 'Account' })).toBeVisible();
        await expect(page.getByRole('button', { name: 'Profile' })).toBeVisible();
        await expect(page.getByText('Logout')).toBeVisible();
    });


});