import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
    await page.waitForTimeout(500);
  });
  
test.describe('AddCollaborator component', () => {

    let context1;
    let context2;
    let page1;
    let page2;

    test.beforeAll(async ({ browser }) => {
        context1 = await browser.newContext();
        context2 = await browser.newContext();
    });

    test.afterAll(async () => {
        await Promise.all([
            context1.close(),
            context2.close()
        ]);
    });

    test('user1 adds user2 as collaborator to list', async ({ context }) => {
        page1 = await context1.newPage();
        await page1.goto('http://localhost:3000/login');
        await page1.getByPlaceholder('Enter email').click();
        await page1.getByPlaceholder('Enter email').fill('testing@email.com');
        await page1.getByPlaceholder('Enter password').click();
        await page1.getByPlaceholder('Enter password').fill('Password1!');
        await page1.locator('form').getByRole('button', { name: 'Login' }).click();

        await page1.waitForTimeout(1000);
        await page1.getByRole('button', { name: 'Account' }).click();
        await page1.getByRole('button', { name: 'Profile' }).click();
        await page1.locator('li').filter({ hasText: 'testing list' }).click();
        await expect(page1.getByRole('heading', { name: 'testing list' })).toBeVisible();
        await page1.getByRole('button', { name: 'Add Collaborator' }).click();
        await expect(page1.getByRole('heading', { name: 'Add a Collaborator to List' })).toBeVisible();
        await expect(page1.locator('li').filter({ hasText: /^testing1$/ })).toBeVisible();
        await page1.locator('li').filter({ hasText: /^testing1$/ }).click();
        await page1.waitForTimeout(1000);
        await page1.locator('li').filter({ hasText: 'testing list' }).click();
        await expect(page1.getByRole('heading', { name: 'testing list' })).toBeVisible();
        await expect(page1.getByRole('cell', { name: 'testing', exact: true })).toBeVisible();
        await expect(page1.getByRole('cell', { name: 'testing1' })).toBeVisible();

    });

    test('user2 lists update', async ({ context }) => {
        page2 = await context2.newPage();
        await page2.goto('http://localhost:3000/login');
        await page2.getByPlaceholder('Enter email').click();
        await page2.getByPlaceholder('Enter email').fill('testing1@email.com');
        await page2.getByPlaceholder('Enter password').click();
        await page2.getByPlaceholder('Enter password').fill('Password1!');
        await page2.locator('form').getByRole('button', { name: 'Login' }).click();

        await page2.waitForTimeout(1000);
        await page2.getByRole('button', { name: 'Account' }).click();
        await page2.getByRole('button', { name: 'Profile' }).click();
        await page2.locator('li').filter({ hasText: 'testing list' }).click();
        await expect(page2.getByRole('heading', { name: 'testing list' })).toBeVisible();
        await expect(page2.getByRole('cell', { name: 'testing', exact: true })).toBeVisible();
        await expect(page2.getByRole('cell', { name: 'testing1' })).toBeVisible();
    });
});


test.describe('LeaveList component', () => {

    let context1;
    let context2;
    let page1;
    let page2;

    test.beforeAll(async ({ browser }) => {
        context1 = await browser.newContext();
        context2 = await browser.newContext();
    });

    test.afterAll(async () => {
        await Promise.all([
            context1.close(),
            context2.close()
        ]);
    });

    test('user2 leaves list collaboration', async ({ context }) => {
        page2 = await context2.newPage();
        await page2.goto('http://localhost:3000/login');
        await page2.getByPlaceholder('Enter email').click();
        await page2.getByPlaceholder('Enter email').fill('testing1@email.com');
        await page2.getByPlaceholder('Enter password').click();
        await page2.getByPlaceholder('Enter password').fill('Password1!');
        await page2.locator('form').getByRole('button', { name: 'Login' }).click();

        await page2.waitForTimeout(1000);
        await page2.getByRole('button', { name: 'Account' }).click();
        await page2.getByRole('button', { name: 'Profile' }).click();
        await page2.locator('li').filter({ hasText: 'testing list' }).click();
        await expect(page2.getByRole('heading', { name: 'testing list' })).toBeVisible();
        await expect(page2.getByRole('cell', { name: 'testing', exact: true })).toBeVisible();
        await expect(page2.getByRole('cell', { name: 'testing1' })).toBeVisible();
        page2.on('dialog', async dialog => {
            await dialog.accept();
          });
        await page2.getByRole('button', { name: 'Leave List' }).click();
        await expect(page2.locator('li').filter({ hasText: 'testing list' })).toBeHidden();
        await expect(page2.getByText('Number of Lists: No lists found')).toBeVisible();
    })
    test('user1 lists update', async ({ context }) => {
        page1 = await context1.newPage();
        await page1.goto('http://localhost:3000/login');
        await page1.getByPlaceholder('Enter email').click();
        await page1.getByPlaceholder('Enter email').fill('testing@email.com');
        await page1.getByPlaceholder('Enter password').click();
        await page1.getByPlaceholder('Enter password').fill('Password1!');
        await page1.locator('form').getByRole('button', { name: 'Login' }).click();

        await page1.waitForTimeout(1000);
        await page1.getByRole('button', { name: 'Account' }).click();
        await page1.getByRole('button', { name: 'Profile' }).click();
        await page1.locator('li').filter({ hasText: 'testing list' }).click();
        await expect(page1.getByRole('heading', { name: 'testing list' })).toBeVisible();
        await expect(page1.getByRole('cell', { name: 'testing', exact: true })).toBeVisible();
        await expect(page1.getByRole('cell', { name: 'testing1' })).toBeHidden();
    });

});