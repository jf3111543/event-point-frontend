import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
    await page.goto('http://localhost:3000/login');
    await page.waitForTimeout(500);
});


test.describe('Login component', () => {

    test('renders login form when page loads', async ({ page }) => {
        await expect(page.getByRole('heading', { name: 'Login' })).toBeVisible();
    });

    test('empty fields validation', async ({ page }) => {
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await expect(page.getByText('Please enter a valid email address')).toBeVisible();
        await expect(page.getByText('Password must be at least 6 characters long')).toBeVisible();
    });

    test('email field validation', async ({ page }) => {
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('test');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('password');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await expect(page.getByText('Please enter a valid email address')).toBeVisible();
    });

    test('no account found message', async ({ page }) => {
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('test@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('password');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await expect(page.getByText('No account found')).toBeVisible();
    });

    test('button to signup page', async ({ page }) => {
        await page.getByRole('link', { name: 'Signup' }).click();
        await expect(page.getByRole('heading', { name: 'Create an Account' })).toBeVisible();
    });

    test('incorrect password', async ({ page }) => {
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('wrongpassword');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await expect(page.getByText('Incorrect password')).toBeVisible();
    });

    test('successful login', async ({ page }) => {
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByLabel('Account').getByRole('button', { name: 'Account' }).click();
        await expect(page.getByText('Username: testing')).toBeVisible();
    });

});