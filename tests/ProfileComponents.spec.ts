import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
    await page.goto('http://localhost:3000/login');
    await page.getByPlaceholder('Enter email').click();
    await page.getByPlaceholder('Enter email').fill('testing@email.com');
    await page.getByPlaceholder('Enter password').click();
    await page.getByPlaceholder('Enter password').fill('Password1!');
    await page.locator('form').getByRole('button', { name: 'Login' }).click();
    await page.waitForTimeout(1000);
});

test.describe('ViewProfile component', () => {

    test('renders profile when page loads', async ({ page }) => {
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Profile' }).click();
        await expect(page.getByRole('heading', { name: 'testing' })).toBeVisible();
        await expect(page.getByRole('heading', { name: 'Lists' })).toBeVisible();
        await expect(page.getByRole('heading', { name: 'Friends' })).toBeVisible();
        await expect(page.getByRole('button', { name: 'Edit' })).toBeVisible();
    });

});


test.describe('SearchUser component', () => {

    test('renders profiles when page loads', async ({ page }) => {
        await page.getByRole('button', { name: 'Users' }).click();
        await expect(page.getByPlaceholder('Search Users...')).toBeVisible();
        await expect(page.getByRole('heading', { name: 'testing2' })).toBeVisible();
    });

    test('shows profile information of selected user', async ({ page }) => {
        await page.getByRole('button', { name: 'Users' }).click();
        await page.getByRole('button', { name: 'View Profile' }).first().click();
        await expect(page.getByRole('heading', { name: 'testing1' })).toBeVisible();
    });

    test('filters users by search input', async ({ page }) => {
        await page.getByRole('button', { name: 'Users' }).click();
        await page.getByPlaceholder('Search Users...').click();
        await page.waitForTimeout(1000);
        await page.getByPlaceholder('Search Users...').fill('2');
        await expect(page.getByRole('heading', { name: 'testing2' })).toBeVisible();
    });
    test('shows no users when no users match search input', async ({ page }) => {
        await page.getByRole('button', { name: 'Users' }).click();
        await page.getByPlaceholder('Search Users...').click();
        await page.waitForTimeout(1000);
        await page.getByPlaceholder('Search Users...').fill('no users');
        await expect(page.getByText('No results found')).toBeVisible();
    });
});
