import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
    await page.waitForTimeout(250);
  });

test.describe('CreateList component', () => {

    test.beforeEach(async ({ page }) => {
        await page.goto('http://localhost:3000/login');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Profile' }).click();
    });

    test('create a new list', async ({ page }) => {
        await page.getByRole('button', { name: 'Create new list' }).click();
        await expect(page.locator('div').filter({ hasText: 'Create Custom User ListList' }).nth(3)).toBeVisible();
        await page.getByRole('textbox').click();
        await page.getByRole('textbox').fill('test list');
        await page.locator('input[name="visibility"]').check();
        await page.getByRole('button', { name: 'Create', exact: true }).click();
        await expect(page.locator('li').filter({ hasText: 'test list' })).toBeVisible();
    });

    test('view list information', async ({ page }) => {
        await page.getByText('test list').click();
        await expect(page.getByRole('heading', { name: 'test list' })).toBeVisible();
        await page.getByRole('button', { name: 'Close' }).click();
    });


});

test.describe('AddToList component', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('http://localhost:3000/login');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
    });
    test('cancel save event to list', async ({ page }) => {
        await page.getByRole('button', { name: 'Save to List' }).first().click();
        await expect(page.getByRole('dialog')).toBeVisible();
        await expect(page.getByRole('heading', { name: 'Add to List' })).toBeVisible();
        await page.getByRole('button', { name: 'Cancel' }).click();

    });

    test('save event to list', async ({ page }) => {
        await page.getByRole('button', { name: 'Save to List' }).first().click();
        await expect(page.getByRole('dialog')).toBeVisible();
        await expect(page.getByRole('heading', { name: 'Add to List' })).toBeVisible();
        await page.getByText('test list').click();
    });
    test('show event saved to list', async ({ page }) => {
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Profile' }).click();
        await page.locator('li').filter({ hasText: 'test list' }).click();
        await expect(page.getByRole('heading', { name: 'test list' })).toBeVisible();
        await expect(page.getByText('No Events found')).toBeHidden();
    });
    test('show event information from list', async ({ page }) => {
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Profile' }).click();
        await page.locator('li').filter({ hasText: 'test list' }).click();
        await expect(page.getByRole('heading', { name: 'test list' })).toBeVisible();
        await page.getByRole('button', { name: 'View' }).click();
        await expect(page.getByRole('dialog').nth(1)).toBeVisible();
    });
    test('cancel confirm prompt to remove event from list', async ({ page }) => {
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Profile' }).click();
        await page.locator('li').filter({ hasText: 'test list' }).click();
        await expect(page.getByRole('heading', { name: 'test list' })).toBeVisible();
        page.on('dialog', async dialog => {
            await dialog.dismiss();
          });
        await page.getByRole('button', { name: 'Remove' }).click();
    });
    test('accept confirm prompt to remove event from list', async ({ page }) => {
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Profile' }).click();
        await page.locator('li').filter({ hasText: 'test list' }).click();
        await expect(page.getByRole('heading', { name: 'test list' })).toBeVisible();
        page.on('dialog', async dialog => {
            await dialog.accept();
          });
        await page.getByRole('button', { name: 'Remove' }).click();
        await expect(page.getByRole('heading', { name: 'testing' })).toBeVisible();
    });


});

test.describe('EditList component', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('http://localhost:3000/login');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Profile' }).click();
    });
    
    test('cancel edit list details', async ({ page }) => {
        await page.waitForTimeout(1000);
        await page.locator('li').filter({ hasText: 'test list' }).click();
        await expect(page.getByRole('heading', { name: 'test list' })).toBeVisible();
        await page.getByRole('dialog').getByRole('button', { name: 'Edit' }).click();
        await page.getByRole('button', { name: 'Cancel' }).click();
        await page.waitForTimeout(1500);
        await expect(page.getByRole('heading', { name: 'test list' })).toBeVisible();
        await page.getByRole('button', { name: 'Close' }).click();
    });


    test('edit list details', async ({ page }) => {
        await page.locator('li').filter({ hasText: 'test list' }).click();
        await expect(page.getByRole('heading', { name: 'test list' })).toBeVisible();
        await page.getByRole('dialog').getByRole('button', { name: 'Edit' }).click();
        await page.getByRole('textbox').click();
        await page.getByRole('textbox').fill('test list update');
        await page.getByRole('button', { name: 'Save' }).click();
        await page.waitForTimeout(1500);
        await page.locator('li').filter({ hasText: 'test list update' }).click();
        await expect(page.getByRole('heading', { name: 'test list update' })).toBeVisible();
        await page.getByRole('button', { name: 'Close' }).click();
    });

});
test.describe('DeleteList component', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('http://localhost:3000/login');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Profile' }).click();
    });
    test('cancel confirm prompt to delete list ', async ({ page }) => {
        await page.getByText('test list update').click();
        await expect(page.getByRole('heading', { name: 'test list update' })).toBeVisible();
        await page.getByRole('dialog').getByRole('button', { name: 'Edit' }).click();
        page.on('dialog', async dialog => {
            await dialog.dismiss();
        });
        await page.getByRole('button', { name: 'Delete' }).click();
    });

    test('accept confirm prompt to delete list', async ({ page }) => {
        await page.waitForTimeout(1000);
        await page.getByText('test list update').click();
        await expect(page.getByRole('heading', { name: 'test list update' })).toBeVisible();
        await page.getByRole('dialog').getByRole('button', { name: 'Edit' }).click();
        page.on('dialog', async dialog => {
            await dialog.accept();
        });
        await page.getByRole('button', { name: 'Delete' }).click();
        await page.waitForTimeout(1000);
        await expect(page.getByText('Number of Lists: 1')).toBeVisible();
    });
});
