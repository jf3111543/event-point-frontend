import { test, expect } from '@playwright/test';

/*
    looked at this guide when handling alert and confirm dialogs throughout the test files:
    https://playwright.dev/docs/api/class-dialog#dialog-accept
*/
test.beforeEach(async ({ page }) => {
    await page.waitForTimeout(500);
  });

test.describe('Signup component', () => {

    test('renders signup form when page loads', async ({ page }) => {
        await page.goto('http://localhost:3000/signup');
        await expect(page.getByRole('heading', { name: 'Create An Account' })).toBeVisible();
    });

    test('empty fields validation', async ({ page }) => {
        await page.goto('http://localhost:3000/signup');
        await page.locator('form').getByRole('button', { name: 'Create Account' }).click();
        await expect(page.getByText('Username must be between 3-64 characters long')).toBeVisible();
        await expect(page.getByText('Please enter a valid email address')).toBeVisible();
        await expect(page.getByText('Password must be at least 6 characters long')).toBeVisible();
    });

    test('input validation', async ({ page }) => {
        await page.goto('http://localhost:3000/signup');
        await page.getByPlaceholder('Enter username').click();
        await page.getByPlaceholder('Enter username').fill('t');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing3');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('p');
        await page.getByRole('button', { name: 'Create Account' }).click();
        await expect(page.getByText('Username must be between 3-64')).toBeVisible();
        await expect(page.getByText('Please enter a valid email')).toBeVisible();
        await expect(page.getByText('Password must be at least 6')).toBeVisible();
    });

    test('secure password requirement validation', async ({ page }) => {
        await page.goto('http://localhost:3000/signup');
        await page.getByPlaceholder('Enter username').click();
        await page.getByPlaceholder('Enter username').fill('testing3');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing3@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('password');
        await page.getByRole('button', { name: 'Create Account' }).click();
        await expect(page.getByText('Password must contain at')).toBeVisible();
    });

    test('create an account succesfully', async ({ page }) => {
        await page.goto('http://localhost:3000/signup');
        await page.getByPlaceholder('Enter username').click();
        await page.getByPlaceholder('Enter username').fill('testing3');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing3@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.getByRole('button', { name: 'Create Account' }).click();
        await page.waitForTimeout(1500);
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByLabel('Account').getByRole('button', { name: 'Account' }).click();
        await expect(page.getByText('Username: testing3')).toBeVisible();
    });

    test('create an account with credentials already stored in the database', async ({ page }) => {
        await page.goto('http://localhost:3000/signup');
        await page.getByRole('button', { name: 'Create an Account' }).click();
        await page.getByPlaceholder('Enter username').click();
        await page.getByPlaceholder('Enter username').fill('testing3');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing3@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.getByRole('button', { name: 'Create Account' }).click();
        await expect(page.getByText('Field must be unique')).toBeVisible();
    });

    test('button to login page', async ({ page }) => {
        await page.goto('http://localhost:3000/signup');
        await page.getByRole('link', { name: 'Login' }).click();
        await expect(page.getByRole('heading', { name: 'Login' })).toBeVisible();
    });
});

test.describe('UserAccount component', () => {

    test('renders account when page loads', async ({ page }) => {
        await page.goto('http://localhost:3000/login');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing3@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByLabel('Account').getByRole('button', { name: 'Account' }).click();
        await expect(page.getByRole('heading', { name: 'User Account' })).toBeVisible();
        await expect(page.getByText('Username: testing3')).toBeVisible();
        await expect(page.getByText('Email: testing3@email.com')).toBeVisible();
        
    });

});

test.describe('EditUser component', () => {

    test('input validation', async ({ page }) => {
        await page.goto('http://localhost:3000/login');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing3@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByLabel('Account').getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Edit' }).click();
        await page.waitForTimeout(1000);
        await page.getByPlaceholder('Enter username').click();
        await page.getByPlaceholder('Enter username').fill('t');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing3email.com');
        await page.getByRole('button', { name: 'Save' }).click();
        await page.waitForTimeout(1000);
        await expect(page.getByText('Please enter a valid email')).toBeVisible();
        await expect(page.getByText('Username must be between 3-64')).toBeVisible();
    });

    test('edit user account details', async ({ page }) => {
        await page.goto('http://localhost:3000/login');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing3@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByLabel('Account').getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Edit' }).click();
        await page.waitForTimeout(500);
        await page.getByPlaceholder('Enter username').click();
        await page.waitForTimeout(500);
        await page.getByPlaceholder('Enter username').fill('testing3update');
        await page.waitForTimeout(500);
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing3update@email.com');
        await page.getByRole('button', { name: 'Save' }).click();
        await expect(page.getByRole('heading', { name: 'User Account' })).toBeVisible();
        await expect(page.getByText('Username: testing3update')).toBeVisible();
        await expect(page.getByText('Email: testing3update@email.com')).toBeVisible();
    });

});


test.describe('DeleteUser component', () => {

    test('cancel confirm promt to delete account', async ({ page }) => {
        await page.goto('http://localhost:3000/login');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing3update@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
        await page.getByRole('button', { name: 'Account' }).click();
        await page.waitForTimeout(500);
        await page.getByLabel('Account').getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Edit' }).click();
        await page.waitForTimeout(500);
        await expect(page.getByRole('heading', { name: 'Edit Account' })).toBeVisible();
        page.on('dialog', async dialog => {
            await dialog.dismiss();
          });
        await page.getByRole('button', { name: 'Delete' }).click();
        await expect(page.getByRole('heading', { name: 'Edit Account' })).toBeVisible();
    });

    test('accept confirm promt to delete account', async ({ page }) => {
        await page.goto('http://localhost:3000/login');
        await page.getByPlaceholder('Enter email').click();
        await page.getByPlaceholder('Enter email').fill('testing3update@email.com');
        await page.getByPlaceholder('Enter password').click();
        await page.getByPlaceholder('Enter password').fill('Password1!');
        await page.locator('form').getByRole('button', { name: 'Login' }).click();
        await page.waitForTimeout(1000);
        await page.getByRole('button', { name: 'Account' }).click();
        await page.getByLabel('Account').getByRole('button', { name: 'Account' }).click();
        await page.getByRole('button', { name: 'Edit' }).click();
        await expect(page.getByRole('heading', { name: 'Edit Account' })).toBeVisible();
        page.on('dialog', async dialog => {
            await dialog.accept();
          });
        await page.getByRole('button', { name: 'Delete' }).click();
        await expect(page.getByRole('button', { name: 'Login' })).toBeVisible();
        await expect(page.getByRole('button', { name: 'Create an Account' })).toBeVisible();
    });

});