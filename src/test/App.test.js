import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter, Routes } from 'react-router-dom';
import App from '../App.jsx';
import * as cookieConfig from '../cookieConfig.js'; // Import your cookieConfig file

jest.mock('../cookieConfig'); // Mock the cookieConfig module
// //set up mock data in localStorage
// const localStorageMock = {
//   getItem: jest.fn(),
//   setItem: jest.fn(),
//   removeItem: jest.fn(),
// };
// global.localStorage = localStorageMock;

describe('App Component', () => {

  test('ViewEvents component is rendered', () => {
    render(
      <MemoryRouter initialEntries={['/']}>
        <App />
      </MemoryRouter>
    );

    const viewEventsComponent = screen.getByTestId('view-events-component');
    expect(viewEventsComponent).toBeInTheDocument();
  });


  test('Signup component is rendered', () => {
    render(
      <MemoryRouter initialEntries={['/signup']}>
        <App />
      </MemoryRouter>
    );

    const signupComponent = screen.getByTestId('signup-component');
    expect(signupComponent).toBeInTheDocument();
  });


  test('Login component is rendered', () => {
    render(
      <MemoryRouter initialEntries={['/login']}>
        <App />
      </MemoryRouter>
    );

    const loginComponent = screen.getByTestId('login-component');
    expect(loginComponent).toBeInTheDocument();
  });

  test('UserAccount component is rendered', () => {
    render(
      <MemoryRouter initialEntries={['/account']}>
        <App />
      </MemoryRouter>
    );

    const userAccountComponent = screen.getByTestId('user-account-component');
    expect(userAccountComponent).toBeInTheDocument();
  });

  test('UserProfile component is rendered', () => {
    cookieConfig.getCookie.mockReturnValueOnce('username');
    cookieConfig.getCookie.mockReturnValueOnce('access_token');
    render(
      <MemoryRouter initialEntries={['/profile']}>
        <App />
      </MemoryRouter>
    );

    const userProfileComponent = screen.getByTestId('user-profile-component');
    expect(userProfileComponent).toBeInTheDocument();
  });

  test('EditUser component is rendered', () => {
    render(
      <MemoryRouter initialEntries={['/account/edit']}>
        <App />
      </MemoryRouter>
    );

    const editUserComponent = screen.getByTestId('edit-user-component');
    expect(editUserComponent).toBeInTheDocument();
  });

  test('SearchUser component is rendered', () => {
    render(
      <MemoryRouter initialEntries={['/users']}>
        <App />
      </MemoryRouter>
    );

    const searchUserComponent = screen.getByTestId('search-user-component');
    expect(searchUserComponent).toBeInTheDocument();
  });

  test('ViewProfile component is rendered', () => {
    render(
      <MemoryRouter initialEntries={[`user/profile/mockUsername`]}>
        <App />
      </MemoryRouter>
    );

    const viewProfileComponent = screen.getByTestId('view-profile-component');
    expect(viewProfileComponent).toBeInTheDocument();
  });

});

test('Redirects to Login when not logged in', () => {
  // Mock getCookie to simulate an unauthenticated user
  cookieConfig.getCookie.mockReturnValueOnce(null);
  cookieConfig.getCookie.mockReturnValueOnce(null);

  render(
    <MemoryRouter initialEntries={['/profile']}>
      <App />
    </MemoryRouter>
  );

  const loginComponent = screen.getByTestId('login-component');
  expect(loginComponent).toBeInTheDocument();
});