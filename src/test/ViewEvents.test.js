import '@testing-library/jest-dom'; // Import this line to extend jest-dom matchers
import React from 'react';
import 'setimmediate';
import { render, screen, act, fireEvent } from '@testing-library/react';
import { MemoryRouter, Routes } from 'react-router-dom';
import ViewEvents from '../components/ViewEvents';
import { getEvents } from '../lib/api/backend-server';

//mock matchMedia
window.matchMedia = jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(),
    removeListener: jest.fn(),
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
}));

jest.mock('../lib/api/backend-server');

describe('ViewEvents Component', () => {
    beforeEach(() => {
        getEvents.mockResolvedValue({
            data: [
                {
                    id: 1,
                    name: 'Test Event 1',
                    images: { url: 'image1.jpg' },
                    venue: [{name: 'Test Venue 1'}],
                    dates: {
                        start: {
                            dateTime: '2024-01-01T18:00:00',
                        },
                    },
                },
                {
                    id: 2,
                    name: 'Test Event 2',
                    images: { url: 'image2.jpg' },
                    venue: [{name: 'Test Venue 2'}],
                    dates: {
                        start: {
                            dateTime: '2024-01-01T19:00:00',
                        },
                    },
                },
                {
                    id: 3,
                    name: 'Test Event 3',
                    images: { url: 'image3.jpg' },
                    venue: [{name: 'Test Venue 3'}],
                    dates: {
                        start: {
                            dateTime: '2024-01-01T19:00:00',
                        },
                    },
                },
                {
                    id: 4,
                    name: 'Test Event 4',
                    images: { url: 'image4.jpg' },
                    venue: [{name: 'Test Venue 4'}],
                    dates: {
                        start: {
                            dateTime: '2024-01-01T19:00:00',
                        },
                    },
                },
                {
                    id: 5,
                    name: 'Test Event 5',
                    images: { url: 'image5.jpg' },
                    venue: [{name: 'Test Venue 5'}],
                    dates: {
                        start: {
                            dateTime: '2024-01-01T19:00:00',
                        },
                    },
                },
            ],
        })
    });

    test('Renders events', async () => {
        
        await act(async () => {
            render(<ViewEvents/>, { wrapper: MemoryRouter });
        });
        expect(screen.getByTestId('view-events-component')).toBeInTheDocument();
        
        //mocked data is being displayed
       const testEvent1Elements = screen.queryAllByText('Test Event 1');
       testEvent1Elements.forEach(element => {
           expect(element).toBeInTheDocument();
       });
   
       const testEvent2Elements = screen.queryAllByText('Test Event 2');
       testEvent2Elements.forEach(element => {
           expect(element).toBeInTheDocument();
       });
    });

    test('Render "No events found" when list is empty', async () => {
        jest.clearAllMocks();
        await getEvents.mockResolvedValue({ data: [] })
        
        await act(async () => {
            render(<ViewEvents/>, { wrapper: MemoryRouter });
        });
        expect(screen.getByTestId('view-events-component')).toBeInTheDocument();

        expect(screen.getByText('No events found')).toBeInTheDocument();
    });

    test('Render filtered events based on search input', async () => {
        await act(async () => {
            render(<ViewEvents/>, { wrapper: MemoryRouter });
        });
        
        expect(screen.getByTestId('view-events-component')).toBeInTheDocument();

        //simulate user input
        act(() => {
            fireEvent.change(screen.getByPlaceholderText('Search Events...'), {
                target: { value: 'Test' },
              });
        });

        const testEvent1Elements = screen.queryAllByText('Test Event 1');
       testEvent1Elements.forEach(element => {
           expect(element).toBeInTheDocument();
       });
    });


    test('Render "No results found" when no events match the search term', async () => {
        await act(async () => {
            render(<ViewEvents/>, { wrapper: MemoryRouter });
        });
        
        expect(screen.getByTestId('view-events-component')).toBeInTheDocument();

        //simulate user input
        act(() => {
            fireEvent.change(screen.getByPlaceholderText('Search Events...'), {
                target: { value: 'example' },
              });
        });

        expect(screen.getByText('No results found')).toBeInTheDocument();
    });

    test('Render filtered events based on venue dropdown filter', async () => {
        await act(async () => {
            render(<ViewEvents/>, { wrapper: MemoryRouter });
        });
        
        expect(screen.getByTestId('view-events-component')).toBeInTheDocument();

        //simulate user input
        act(() => {
            fireEvent.click(screen.getByText('Select Venue'), {
              });
        });
        console.log(screen.debug());
        act(() => {
            fireEvent.click(screen.getByTestId('Test Venue 1'));
        });

        
        const testEvent1Elements = screen.queryAllByText('Test Event 1');
       testEvent1Elements.forEach(element => {
        // console.log(element)
           expect(element).toBeInTheDocument();
       });
    
    });

});
