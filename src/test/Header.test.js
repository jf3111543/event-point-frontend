import '@testing-library/jest-dom';
import React from 'react';
import 'setimmediate';
import { render, screen, act, fireEvent } from '@testing-library/react';
import { MemoryRouter, Routes } from 'react-router-dom';
import Header from '../components/Header';

//mock matchMedia
window.matchMedia = jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(),
    removeListener: jest.fn(),
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
}));
jest.mock('../lib/api/backend-server', () => ({
    getNotifications: jest.fn().mockResolvedValue({ status: 200, data: { success: true, data: [{ data: ['mockNotifiaction']}] } })
}));


describe('Header Component', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    test('Renders correctly when logged out', () => {
        render(<Header />, { wrapper: MemoryRouter });
        expect(screen.getByText('EventPoint')).toBeInTheDocument();
        expect(screen.getByText('Create an Account')).toBeInTheDocument();
        expect(screen.getByText('Login')).toBeInTheDocument();
        expect(screen.queryByText('Account')).not.toBeInTheDocument();
        expect(screen.queryByText('Profile')).not.toBeInTheDocument();
    });

    test('Renders correctly when logged in', async () => {
        //mock cookies
        document.cookie = 'username=mockedUsername;';
        document.cookie = 'access_token=mockedAccessToken;';
    
        await act(async () => {
            render(<Header/>, { wrapper: MemoryRouter });
        });
        
        expect(screen.getByText('EventPoint')).toBeInTheDocument();
        expect(screen.queryByText('Create an Account')).not.toBeInTheDocument();
        expect(screen.queryByText('Login')).not.toBeInTheDocument();
        expect(screen.getByText('Account')).toBeInTheDocument();

        fireEvent.click(screen.getByText('Account'));
        expect(screen.getByText('Profile')).toBeInTheDocument();
        expect(await screen.findByTestId('notification-component')).toBeInTheDocument();

    });
    
});
