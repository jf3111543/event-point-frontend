// import '@testing-library/jest-dom';
// import React from 'react';
// import { render, screen, waitFor, fireEvent } from '@testing-library/react';
// import { act } from 'react-dom/test-utils';
// import { MemoryRouter, Routes } from 'react-router-dom';
// import Signup from '../components/Signup';
// import Login from '../components/Login';
// import { addUser, loginUser } from '../lib/api/backend-server';

// jest.mock('../lib/api/backend-server');

// describe('Signup Component', () => {

//     test('Create a user account then login using the inputted data', async () => {
//         //mock user data
//         const testUser = { username: 'testuser', email: 'test@example.com', password: 'testpassword' };
//         const testToken = 'test-token';

//         //call function and return mocked data
//         addUser.mockResolvedValue({
//             status: 200,
//             data: {
//                 success: true,
//                 data: {
//                     email: testUser.email,
//                     password: testUser.password,
//                     //default values
//                     profile_visible: false,
//                     number_lists: 0,
//                     custom_lists: expect.objectContaining({ placeholder: "placeholder" }),
//                     number_friends: 0,
//                     friends: expect.objectContaining({ placeholder: "placeholder" }),
//                 }
//             }
//         });
//         //call function and return mocked data
//         loginUser.mockResolvedValue({ status: 200, data: { success: true, token: testToken, username: testUser.username } });

//         render(<Signup />, { wrapper: MemoryRouter });

//         //component renders correctly
//         await waitFor(() => {
//             expect(screen.getByTestId('signup-component')).toBeInTheDocument();
//         });

//         //simulate user input
//         fireEvent.change(screen.getByPlaceholderText('Enter username'), { target: { name: 'username', value: testUser.username } });
//         fireEvent.change(screen.getByPlaceholderText('Enter email'), { target: { name: 'email', value: testUser.email } });
//         fireEvent.change(screen.getByPlaceholderText('Enter password'), { target: { name: 'password', value: testUser.password } });

//         fireEvent.click(screen.getByText('Create Account'));

//         //functions called with correct data
//         await waitFor(() => {
//             expect(addUser).toHaveBeenCalledWith(
//                 expect.objectContaining({
//                     email: testUser.email,
//                     password: testUser.password,
//                     //default values
//                     profile_visible: false,
//                     number_lists: 0,
//                     custom_lists: expect.objectContaining({ placeholder: "placeholder" }),
//                     number_friends: 0,
//                     friends: expect.objectContaining({ placeholder: "placeholder" }),


//                 })
//             );
//             expect(loginUser).toHaveBeenCalledWith({ email: testUser.email, password: testUser.password });
//             expect(localStorage.getItem('token')).toBe(testToken);
//             expect(localStorage.getItem('username')).toBe(testUser.username);
//         });
//     });

// });
