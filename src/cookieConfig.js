import Cookies from 'universal-cookie';

const cookies = new Cookies();

// const setCookie = (name, value) => {
//   cookies.set(name, value);
// };

const setCookie = (name, value) => {
  const existingValue = cookies.get(name);

    // Calculate expiration time (1 day from now)
    const expirationDate = new Date();
    expirationDate.setDate(expirationDate.getDate() + 1); // Add 1 day
  
  // Check if the cookie already exists
  if (existingValue !== undefined) {
    // If the cookie exists, update its value
    cookies.set(name, value, { expires: expirationDate });
  } else {
    // If the cookie doesn't exist, create a new one
    cookies.set(name, value, { expires: expirationDate });
  }
};

const getCookie = (name) => {
  return cookies.get(name);
};

const removeCookie = (name) => {
  cookies.remove(name);
};

export { setCookie, getCookie, removeCookie };