import React from 'react';
import { Routes, Route } from 'react-router-dom';


import ViewEvents from './components/ViewEvents';
import Signup from './components/Signup';
import Login from './components/Login';
import UserAccount from './components/UserAccount';
import UserProfile from './components/UserProfile';
import EditUser from './components/EditUser';

import SearchUser from './components/SearchUser';
import Error from './components/Error'
import ViewProfile from './components/ViewProfile';

const App = () => {



  return (
  <div>
    

    <Routes>
      <Route path="/" element={<ViewEvents />} />
      <Route path="/signup" element={<Signup />} />
      <Route path="/login" element={<Login />} />
      <Route path="/account" element={<UserAccount />} />
      <Route path="/profile" element={<UserProfile />} />
      <Route path="/account/edit" element={<EditUser />} />
      <Route path="/users" element={<SearchUser />} />
      <Route path="/error" element={<Error />} />
      <Route path="/user/profile/:selectedUsername" element={<ViewProfile />} />
    </Routes>
  </div>
  );
};
export default App;