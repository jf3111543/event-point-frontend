import applyCaseMiddleware from "axios-case-converter";
import axios from "axios";

const options = {
  ignoreHeaders: true,
};

const frontend = applyCaseMiddleware(
  axios.create({
    baseURL: "https://3.253.123.101:3333/",
    headers: {
      'Content-Type': 'application/json',
    },
  }),
  options
);

export default frontend;

