import frontend from './frontend';
import { getCookie } from '../../cookieConfig';

//gets events from server
export const getEvents = () => {
    return frontend.get('/events');
};

export const getEventInformation = (eventId) => {
    // console.log("getEvenetInformation called")
    try {
        return frontend.get(`/event/${eventId}`);
    } catch (error) {
        console.error('Error getting event information:', error);
        throw error;
    }

}

//creates new user account
export const addUser = (userData) => {
    // console.log("addUser called");
    // console.log(userData);
    const accessToken = getCookie('access_token')
    // console.log(accessToken);

    return frontend.post('/user/add', userData, {headers: {
        'x-access-token': accessToken,
    }});

    
};

//logs in existing user
export const loginUser = (userData) => {
    //console.log("loginUser called");
    //console.log(userData);
    const accessToken = getCookie('access_token')
    // console.log(accessToken);

    return frontend.post('/user/login', userData, {headers: {
        'x-access-token': accessToken,
    }});
};

//logs out user
export const logoutUser = (username, accessToken) => {
    // console.log("logout called");
    // console.log(username);
    // console.log(accessToken);

    return frontend.get(`/user/logout/${username}`, {headers: {
        'x-access-token': accessToken,
    }});
}


//gets user details
export const getUserDetails = (username, accessToken) => {
    // console.log("getUserDetails called");
    // console.log(username);
    // console.log(accessToken);

    return frontend.get(`/user/${username}`, {headers: {
        'x-access-token': accessToken,
    }});

};


//creates new custom user list
export const createCustomUserList = (username, accessToken, listData) => {
    // console.log("createCustomUserList called");
    // console.log(userData);
    // console.log(listData);

    try {
        return frontend.post(`/user/${username}/add/list/`, listData, {headers: {
            'x-access-token': accessToken,
        }});

    } catch (error) {
        console.error('Error creating custom user list:', error);
        throw error;
    }
};


export const addEventToList = (username, accessToken, listData, eventData) => {
    // console.log("addEventToList called");
    // console.log(userData);
    // console.log(listData);
    // console.log(eventData);

    try {
        //return frontend.put(`/user/${username}/lists/${listData}`, { eventData }, { headers: { 'Content-Type': 'application/json' } });

        return frontend.put(`/user/${username}/lists/event/add/${listData}`, { eventData }, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error creating custom user list:', error);
        throw error;
    }
};

//gets lists 
export const getUserLists = (username, accessToken) => {
    console.log("getUserLists called");
    // console.log(listData);
    try {
    return frontend.get(`/user/${username}/lists`, {headers: {
        'x-access-token': accessToken,
    }});
} catch (error) {
    console.error('Error getting user lists:', error);
    throw error;
}
};

//gets list details
export const getListDetails = (username, accessToken, listData) => {
    console.log("getUserDetails called");
    // console.log(listData);
    try {
    return frontend.get(`/user/${username}/lists/${listData}`, {headers: {
        'x-access-token': accessToken,
    }});
} catch (error) {
    console.error('Error getting custom user list:', error);
    throw error;
}
};


export const editCustomUserList = (username, accessToken, listData, updatedListData) => {
    console.log("editCustomUserList called");
    // console.log(username);
    // console.log(listData);
    // console.log(updatedListData);

    try {
        //return frontend.put(`/user/${username}/lists/${listData}`, { eventData }, { headers: { 'Content-Type': 'application/json' } });

        return frontend.put(`/user/${username}/lists/edit/${listData}`, { updatedListData }, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error updating custom user list:', error);
        throw error;
    }
};

export const removeEvent = (username, accessToken, listId, eventId) => {
    console.log("removeEvent called");
    // console.log(username);
    // console.log(listData);
    // console.log(updatedListData);

    try {
        //return frontend.put(`/user/${username}/lists/${listData}`, { eventData }, { headers: { 'Content-Type': 'application/json' } });

        return frontend.delete(`/user/${username}/lists/${listId}/event/remove/${eventId}`, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error removing event from custom user list:', error);
        throw error;
    }
};

export const deleteCustomList = (username, accessToken, listData) => {
    console.log("deleteCustomList called");
    console.log(username);
    console.log(listData);
    console.log(accessToken)

    try {
        //return frontend.put(`/user/${username}/lists/${listData}`, { eventData }, { headers: { 'Content-Type': 'application/json' } });

        return frontend.delete(`/user/${username}/lists/delete/${listData}`, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error deleting custom user list:', error);
        throw error;
    }
};

export const addCollaborator = (username, accessToken, listId, collaboratorUsername) => {
    console.log("addCollaborator called");
    console.log(accessToken);
    console.log(listId);
    console.log(collaboratorUsername);

    try {
        //return frontend.put(`/user/${username}/lists/${listData}`, { eventData }, { headers: { 'Content-Type': 'application/json' } });

        return frontend.put(`/user/${username}/lists/${listId}/collaborator/add/${collaboratorUsername}`, {}, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error adding collaborator to custom user list:', error);
        throw error;
    }
};

export const leaveCustomList = (username, accessToken, listId) => {
    console.log("leaveCustomList called");
    console.log(username);
    console.log(listId);
    console.log(accessToken)

    try {
        //return frontend.put(`/user/${username}/lists/${listData}`, { eventData }, { headers: { 'Content-Type': 'application/json' } });

        return frontend.put(`/user/${username}/lists/${listId}/collaborator/remove`, {}, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error deleting custom user list:', error);
        throw error;
    }
};

export const editUserDetails = (username, accessToken, updatedUserData) => {
    console.log("editUserDetails called");
    // console.log(username);
    // console.log(listData);
    // console.log(updatedListData);

    try {
        //return frontend.put(`/user/${username}/lists/${listData}`, { eventData }, { headers: { 'Content-Type': 'application/json' } });

        return frontend.put(`/user/edit/${username}`, { updatedUserData }, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error updating user details:', error);
        throw error;
    }
}

export const deleteUserDetails = (username, accessToken) => {
    console.log("deleteUser called");
    console.log(username);
    console.log(accessToken)

    try {
        //return frontend.put(`/user/${username}/lists/${listData}`, { eventData }, { headers: { 'Content-Type': 'application/json' } });

        return frontend.delete(`/user/delete/${username}`, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error deleting user:', error);
        throw error;
    }
};


export const getUsers = () => {
    const username = getCookie('username')
    const accessToken = getCookie('access_token')

    try{
    return frontend.get('/users', {headers: {
        'x-access-token': accessToken,
    }});
    } catch (error) {
        console.log('Error getting users: ', error);
        throw error
    }
};

export const getUsersProfiles = (username, accessToken) => {
    console.log("getUsersProfiles called");
    // console.log(username);
    // console.log(accessToken)
    try{
        //this is a post request to pass the username in the body which cant be done with a get
        return frontend.post(`/users/profiles`, {username}, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.log('Error getting users profiles: ', error);
        throw error
    }
};

export const getSelectedProfile = (username, accessToken, selectedUsername) => {
    console.log("getSelectedProfile called");
    // console.log(username);
    // console.log(accessToken)
    try{
        return frontend.get(`/user/profile/${selectedUsername}`, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.log('Error getting user profile: ', error);
        throw error
    }
};

export const sendFriendRequest = (username, accessToken, friendRequestData) => {
    console.log("sendFriendRequest called");
    // console.log(username);
    // console.log(accessToken)
    // console.log(friendRequestData)
    try {
        return frontend.post(`/friendrequest`, friendRequestData, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error sending friend request:', error);
        throw error;
    }
}

export const friendRequestResponse = (username, accessToken, requestId, response) => {
    console.log("friendRequestResponse called")
    // console.log(requestId)
    // console.log(response)

    try {
        return frontend.put(`/friendrequest/${requestId}`, response, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error sending friend request:', error);
        throw error;
    }

}

export const notificationClear = (username, accessToken, notificationData) => {
    console.log("notificationClear called")
    // console.log(notificationData)
    // console.log(response)

    try {
        return frontend.put(`/user/${username}/notification/clear`, {notificationData}, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error clearing notification:', error);
        throw error;
    }

}

export const removeFriend = (username, accessToken, selectedUsername) => {
    console.log("removeFriend function called")
    // console.log(selectedUsername)
    // console.log(response)

    try {
        return frontend.put(`/user/${username}/friends/${selectedUsername}/remove`, {}, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error sending friend request:', error);
        throw error;
    }

}

export const getNotifications = (username, accessToken) => {
    console.log("getNotifications called")
    // console.log(requestId)
    // console.log(response)

    try {
        return frontend.get(`/user/${username}/notifications`, {headers: {
            'x-access-token': accessToken,
        }});
    } catch (error) {
        console.error('Error getting notifications:', error);
        throw error;
    }

}