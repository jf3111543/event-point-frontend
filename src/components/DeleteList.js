import React from 'react';
import { deleteCustomList } from '../lib/api/backend-server';
import { getCookie } from '../cookieConfig';

function DeleteList({listId}) {

    const handleDelete = async () => {
        try {
            console.log("handle delete function called")
            const username = getCookie('username')
            const accessToken = getCookie('access_token')

            const response = await deleteCustomList(username, accessToken, listId)
            if (response.status == 200) {
                if (response.data.success == true) {
                    console.log("List deleted");
                    console.log(response.data.data)
                    window.location.reload()

                }
            } else {
                console.log(response.data.message);
            }

        } catch (error) {
            console.log(error)
        }

    }

    const confirmDelete = () => {
        const isConfirmed = window.confirm("Are you sure you want to delete this list?");
        if (isConfirmed) {
            handleDelete();
        }
    }


    return (
        <div>
            <button className="btn btn-2" onClick={confirmDelete} >Delete</button>
        </div>
    );
};

export default DeleteList;