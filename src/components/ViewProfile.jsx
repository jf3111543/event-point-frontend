import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Button, Modal, ModalHeader } from 'react-bootstrap';
import { getCookie } from '../cookieConfig';
import Header from './Header';
import { getSelectedProfile } from '../lib/api/backend-server';
import SendFriendRequest from './SendFriendRequest';
import RemoveFriend from './RemoveFriend';

function ViewProfile() {
    const { selectedUsername } = useParams();
    console.log(selectedUsername)

    //holds the user data
    const [userProfile, setUserProfile] = useState([]);
    const [list, setList] = useState([]);
    const [friendsCheck, setFriendsCheck] = useState(false);

    //react Router hook for navigation
    const navigate = useNavigate();

    //fetch the list of events when the component mounts
    useEffect(() => {
        fetchUserProfile();
    }, [selectedUsername]);

    const fetchUserProfile = async () => {
        try {
            console.log(selectedUsername)
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            if (!!username && !!accessToken) {
                const details = await getSelectedProfile(username, accessToken, selectedUsername);
                if (details.status = 200) {
                    if (details.data.success == true) {
                        // console.log("const detaiils:")
                        console.log(details.data)
                        setUserProfile(details.data.data)
                        console.log(details.data.data)
                        //map through details.data.data.friends array and check if the friendsUsername matches the username const 
                        const isFriend = details.data.data.friends.some(friend => friend.friendUsername === username);
                        setFriendsCheck(isFriend)

                    }
                    else {
                        console.log(details.data.message)
                    }
                }
            }
            else {
                console.log("User is not logged in");
                navigate('/')

            }
        } catch (error) {
            console.log(error);
        }
    };

    const showListDetails = (list) => {
        try{
            setList(list)
            setShow(true)
        } catch (error){
            console.log(error)
        }
    }
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    // const handleClose = () => setShow(false);
    // const handleShow = () => setShow(true);

    const handleViewEvent = (eventId) => {
        // Implement the view event functionality using eventId
        console.log("Viewing event with ID:", eventId);
        // Example: Redirect to a page to view event details
      };


    return (
        <div data-testid="view-profile-component">
            <div className="container-fluid justify-content-center align-items-center" id="wrapper">

                <Header />
                <div className="container-fluid">
                <div className="row align-items-center main-content">
                    <div className="row user-profile-header">
                        <div className="col profile-img-col">
                            <img src={`${process.env.PUBLIC_URL}/logo-dark.jpg`} alt="profile picture placeholder" width="200" height="200" />
                        </div>
                        <div className="col profile-information-col">
                        <h1> {userProfile.username} </h1>
                            {!friendsCheck && <SendFriendRequest receiver_username={userProfile.username} />}
                            {friendsCheck && <RemoveFriend selected_username={userProfile.username} />}
                            
                            
                        </div>
                    </div>

                    <div className="row friend-list-row">
                        <div className="col list-col">
                            <div className="list-title">
                            <h1> Lists </h1>
                            </div>
                            <div className="list-text">
                            <p> Number of Lists: {userProfile.numberLists > 0 ? userProfile.numberLists : 'no lists found'} </p>
                            </div>
                            <ul className="list-ul">

                                {userProfile && userProfile.customLists && userProfile.customLists.map((list, index) => (
                                    <li className="list-li" key={index} onClick={() => showListDetails(list)} >
                                        <p className="list-li-text" onClick={handleShow}>
                                            {list.name}
                                        </p>
                                    </li>
                                ))}
                                <Modal show={show} onHide={handleClose} centered>
                                    <ModalHeader className="list-modal-header">
                                        <h1> {list.name} </h1>
                                    </ModalHeader>
                                    <Modal.Body className="list-modal-body">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    {list.collaborators && list.collaborators.length > 0 ? (    
                                                        list.collaborators.map((collaborator, index) => (
                                                            <td key={index}>
                                                                {collaborator.username !== undefined ? collaborator.username : 'Username not found'}
                                                            </td>
                                                        ))
                                                    ) : (
                                                        <></>
                                                    )}
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p> Events: </p>
                                        <ul className="list-ul">
                                            {list.events && list.events.length > 0 ? (    
                                                list.events.map((event, index) => (
                                                    <li className="list-event-li" key={index}>
                                                        {event.eventId !== undefined ? event.eventId : 'No Events found'}
                                                        <button className="btn btn-6" variant="primary" onClick={() => handleViewEvent(event.eventId)}>View</button>
                                                    </li>
                                                ))
                                            ) : (
                                                <li>No Events found</li>
                                            )}
                                        </ul>
                                    </Modal.Body>
                                    <Modal.Footer className="list-modal-header">
                                        <Button className="btn btn-7" onClick={handleClose}>
                                            Close
                                        </Button>
                                    </Modal.Footer>
                                </Modal>

                            </ul>
                        </div>

                        <div className="col friend-col">
                            <div className="friend-title">
                            <h1> Friends</h1>
                            </div>
                            <div className="friend-text">
                            <p> Number of Friends: {userProfile.numberFriends > 0 ? userProfile.numberFriends : 'no friends found'} </p>
                            </div>
                            <ul className="friend-ul">
                                {userProfile && userProfile.friends && userProfile.friends.map((friend, index) => (
                                    <li className="btn friend-li" key={index}>
                                        <p className="friend-li-text" onClick={() => navigate(`/user/profile/${friend.friendUsername}`)}>{friend.friendUsername} </p>
                                    </li>
                                ))}
                            </ul>
                        </div>

                    </div>
                </div>

                <div className="row align-items-center footer">
                    <span>&copy; EventPoint</span>
                </div>

            </div>
            </div>
        </div>
    );
};



export default ViewProfile;