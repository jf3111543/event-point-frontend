import { useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { getCookie } from '../cookieConfig';
import { getEventInformation } from '../lib/api/backend-server';
import AddToList from './AddToList';

function EventInformation({ show, handleClose, eventId }) {
    // console.log("eventId in EventInformation:", eventId);

    const navigate = useNavigate();

    useEffect(() => {
        // fetchUserDetails();
        fetchEventInformation();
    }, []);
    const [event, setEvent] = useState([]);

    const fetchEventInformation = async () => {
        try {
            // console.log("fetchEventInformation function called")
            // const event_id = eventId
            // console.log("details in fetchEventInformation", eventId)
            const details = await getEventInformation(eventId);
            // console.log(details)
            if (details.status = 200) {
                if (details.data.success == true) {
                    // console.log("const detaiils:")
                    // console.log(details.data)
                    setEvent(details.data.data)
                    // console.log(details.data.data)
                    // return details
                }
                else {
                    console.log(details.data.message)
                }
            }
        } catch (error) {
            console.log(error);
        }
    };


    //fetch the list of events when the component mounts
    useEffect(() => {
        checkLoginStatus();
    }, []);

    const [loggedIn, setLoggedIn] = useState('');
    const checkLoginStatus = () => {
        try {
            const username = getCookie('username')
            const accessToken = getCookie('access_token')
            if (!username && !accessToken) {
                // console.log("User is not logged in")
                setLoggedIn(false)
            }
            else {
                // console.log("User is logged in")
                setLoggedIn(true)

            }

        } catch (error) {
            console.log(error)
        }

    }

    const [showAddToList, setShowAddToList] = useState(false);
    const handleEventSave = (eventId) => {
        if (loggedIn) {
            //check eventId is not undefined
            if (eventId !== undefined) {
                setShowAddToList(true);
            }
        } else {
            // console.log("User is not logged in")
            window.alert('You need to be logged in to perform this action')
        }
    };



    return (
        <div>
            {event && (
                <div>
                    <Modal size="lg" show={show} onHide={handleClose} centered>
                        <Modal.Header className="event-information-modal-header">
                            <h1>{event.name}</h1>
                        </Modal.Header>
                        <Modal.Body className="event-information-modal-body">
                            <div className="container-fluid event-information-container">
                                <div className="row event-information-row">
                                    <div className="event-information-img">
                                    <img src={event.images?.url || `${process.env.PUBLIC_URL}/default-event-img.jpg`} alt="the event" />
                                    </div>
                                </div>
                                <div className="row event-information-row">
                                    <div className="event-information-text">
                                    {Array.isArray(event.description) && event.description.length > 0 ? (
                                        <div>
                                        <h5>Description</h5>
                                        <p>{event.description[0]}</p>
                                        </div>
                                    ) : event.description ? (
                                        <div>
                                        <h5>Description</h5>
                                        <p>{event.description}</p>
                                        </div>
                                    ) : (
                                        <p>No description available</p>
                                    )}
                                    </div>
                                </div>
                                <div className="row event-information-row">
                                    {event.venue && event.venue[0] && (
                                        <div className="event-information-text">
                                            {event.venue && event.venue[0] && (
                                                <div>
                                                    <h5>Venue</h5>
                                                    <p>{event.venue[0].name}</p>
                                                    {typeof event.venue[0].address === 'object' ? (
                                                        <>
                                                            <p>{event.venue[0].address.line1}, {event.venue[0].address.line2}</p>
                                                        </>
                                                    ) : (
                                                        <p>{event.venue[0].address || 'No address available'}</p>
                                                    )}
                                                </div>
                                            )}
                                        </div>
                                    )}
                                </div>
                                <div className="row event-information-row">
                                    <div className="event-information-text">
                                    {event.dates && event.dates.start && event.dates.start.dateTime && (
                                        <div>
                                            <h5>Dates</h5>
                                            <p> Start Date: {event.dates?.start?.dateTime ? event.dates.start.dateTime.slice(0, 10) : String(event.dates?.start?.dateTime).slice(0, 10)} </p>
                                            <p> Time: {event.dates?.start?.dateTime ? event.dates.start.dateTime.slice(11, 16) : String(event.dates?.start?.dateTime).slice(11, 16)} </p>
                                        </div>
                                    )}
                                    {event.dates && event.dates.end && event.dates.end.dateTime && (
                                        <p>End Date: {event.dates?.end?.dateTime ? event.dates.end.dateTime.slice(0, 10) : String(event.dates?.end?.dateTime).slice(0, 10)}</p>
                                    )}
                                    </div>
                                </div>
                                <div className="row event-information-row">
                                    {event.note ? (
                                        <div className="event-information-text">
                                            <h5>Note</h5>
                                        <p>{event.note}</p>
                                        </div>
                                    ) : (
                                        <></>
                                    )}
                                </div>
                                <div className="row event-information-row">
                                    {event.url ? (
                                        <div className="event-information-text">
                                        <a className="btn btn-3" href={event.url} target="_blank" rel="noopener noreferrer">View In External Site</a>
                                        </div>
                                    ) : (
                                        <></>
                                    )}
                                </div>
                                {/* sales?*/}
                            </div>
                        </Modal.Body>
                        <Modal.Footer className="event-information-modal-footer">
                            <button className="btn btn-6" onClick={() => handleEventSave(event.id)} >
                                Save to List
                            </button>
                            <Button className="btn btn-7" onClick={handleClose}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    {showAddToList && (
                        <AddToList show={showAddToList} handleClose={() => setShowAddToList(false)} eventId={event.id} />
                    )}
                </div>
            )}
        </div>
    );
}

export default EventInformation;
