import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Logout from './Logout';
import { getCookie } from '../cookieConfig';
import io from 'socket.io-client';
import Notification from './Notification';
import { getNotifications } from '../lib/api/backend-server';
import { Dropdown } from 'react-bootstrap'

function Header() {

    //react Router hook for navigation
    const navigate = useNavigate();

    //fetch the login status when the component mounts
    useEffect(() => {
        checkLoginStatus();
    }, []);

    const [loggedIn, setLoggedIn] = useState('');
    const checkLoginStatus = () => {
        try {
            const username = getCookie('username')
            const accessToken = getCookie('access_token')
            if (!username && !accessToken) {
                // console.log("User is not logged in")
                setLoggedIn(false)
            }
            else {
                // console.log("User is logged in")
                setLoggedIn(true)
            }
        } catch (error) {
            console.log(error)
        }
    }

    const [notifications, setNotifications] = useState([]);

    // Function to fetch pending notifications from the server
    const fetchPendingNotifications = async () => {
        try {

            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            if (!!username && !!accessToken) {
                const response = await getNotifications(username, accessToken)
                console.log(response.data)
                if (response.status = 200) {
                    if (response.data.success == true) {
                        let notification = response.data.data
                        if (!!notification) {
                            console.log(response.data.data[0].data[0])

                            const pendingNotifications = [];
                            response.data.data.forEach(item => {
                                pendingNotifications.push(item.data);
                            });
                            return pendingNotifications;
                        }
                        else {
                            console.log("No notifications found")
                            return [];
                        }
                    }
                    else {
                        console.log(response.data.message)
                    }
                }
            }
            else {
                console.log("User is not logged in");

            }
        } catch (error) {
            console.log(error);
            return [];
        }
    };

    // Fetch pending notifications when the component mounts
    useEffect(() => {
        const fetchNotifications = async () => {
            try {
                const username = getCookie('username');
        const accessToken = getCookie('access_token');

        if (username && accessToken) {
                const notifications = await fetchPendingNotifications();
                console.log(notifications)
                setNotifications(notifications);
        } else{
            setNotifications([])
        }
            } catch (error) {
                console.log(error)
                setNotifications([])
            }
        };
        fetchNotifications();
    }, []); // Runs when component is mounted

    useEffect(() => {
        const username = getCookie('username');
        const accessToken = getCookie('access_token');

        if (username && accessToken) {
            const socket = io('https://3.253.123.101:3333/', {
                auth: {
                    user: username,
                    accessToken: accessToken
                }
            });

            socket.on('friendRequestReceived', (senderUsername) => {
                setNotifications((prevNotifications) => [...prevNotifications, senderUsername]);
                // console.log(notifications)
            });

            socket.on('clearNotification', ({ requestId }) => {
                setNotifications((prevNotifications) => prevNotifications.filter(notification => notification[4] !== requestId));
            });

            return () => {
                socket.disconnect();
            };
        }
    }, []);//runs when component is mounted


    return (
        <div className="row align-items-center header">
            <div className="header-img">
                <img src={`${process.env.PUBLIC_URL}/logo-light.jpg`} alt="logo" width="150" height="150" />
            </div>
            <div className="header-title">
                <h1>EventPoint</h1>
            </div>
            <div className="header-link">
                <div className="header-button-container">
                    <button className="btn btn-8" onClick={() => navigate('/')}> Home </button>
                </div>
                {!loggedIn &&
                    <div className="header-button-container">
                        <button className="btn btn-8" onClick={() => navigate('/signup')}> Create an Account </button>
                    </div>
                }
                {!loggedIn &&
                    <div className="header-button-container">
                        <button className="btn btn-8" onClick={() => navigate('/login')}> Login </button>
                    </div>
                }
                {loggedIn &&
                    <div className="header-button-container">
                        <Dropdown className="header-dropdown">
                            <Dropdown.Toggle className="btn btn-8" id="dropdown-basic">
                                Account
                            </Dropdown.Toggle>
                            <Dropdown.Menu className="header-dropdown-menu">
                                <Dropdown.Item className="header-dropdown-option" onClick={() => navigate('/account')}>Account</Dropdown.Item>
                                <Dropdown.Item className="header-dropdown-option" onClick={() => navigate('/profile')}>Profile</Dropdown.Item>
                                <Dropdown.Item className="header-dropdown-option" ><Logout /></Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                }
                {loggedIn &&
                    <div className="header-button-container">
                        <button className="btn btn-8 search-users-btn" onClick={() => navigate('/users')}>
                        <img src={`${process.env.PUBLIC_URL}/search-icon.png`} width="25px" height="25px" />
                            Users
                            
                        </button>
                    </div>
                }
                {loggedIn &&
                    <div className="header-button-container">
                        <Notification notifications={notifications} />
                    </div>
                }
            </div>
        </div>
    )
}

export default Header;