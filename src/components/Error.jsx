import React from 'react';
import { useLocation } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

function Error() {

    const navigate = useNavigate();

    const location = useLocation();
    const errorMessage = location.state?.data;

    console.log(errorMessage)

    return (
        <div>
        <div className="row align-items-center header">
            <div className="header-img">
                <img src={`${process.env.PUBLIC_URL}/logo-light.jpg`} alt="logo" width="150" height="150" />
            </div>
            <div className="header-title">
                <h1>EventPoint</h1>
            </div>
           
        </div>
        <div className="row">
            <div className="row">
                <h1> Error </h1>
            </div>
            <div className="row">
                <div>
                <p>{errorMessage}</p>
                <button className="" onClick={() => navigate('/')}>Return to Home </button>
                </div>
            </div>
        </div>



        </div>
    )
}

export default Error;