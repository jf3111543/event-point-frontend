import { useEffect, useState } from 'react';
import { Carousel } from 'react-bootstrap';
import { getEvents } from '../lib/api/backend-server';

function EventCarousel() {

    const [carouselEvents, setCarouselEvents] = useState([])

    //fetch the list of events when the component mounts
    useEffect(() => {
        const fetchEvents = async () => {
            try {
                const response = await getEvents();
                const events = response.data;
                const eventsWithImages = events.filter(event => event.images && event.images.url);
            
                const shuffledEvents = eventsWithImages.sort(() => Math.random() - 0.5); // Shuffle events array
                const selectedEvents = shuffledEvents.slice(0, 5); // Select first 5 events
                setCarouselEvents(selectedEvents);
            } catch (error) {
                console.error(error);
            }
        };

        fetchEvents();
    }, []);

    return (
        <Carousel>
            {carouselEvents.map((event, index) => (
                <Carousel.Item key={index}>
                    <div className="carousel-img">
                        <img className="image-carousel" src={event.images?.url || `${process.env.PUBLIC_URL}/default-event-img.jpg`} alt={event.name} />
                    </div>
                        <Carousel.Caption>
                            {event.name}
                            
                        </Carousel.Caption>
                </Carousel.Item>
            ))}
        </Carousel>
    );
}

export default EventCarousel;