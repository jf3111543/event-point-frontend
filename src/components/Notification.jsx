import React, { useState } from 'react';
import { Button, Modal, Offcanvas } from 'react-bootstrap';
import { friendRequestResponse } from '../lib/api/backend-server';
import { setCookie, getCookie, removeCookie } from '../cookieConfig';
import { notificationClear } from '../lib/api/backend-server';

const Notification = ({ notifications }) => {
    const [showModal, setShowModal] = useState(false);
    const [selectedNotification, setSelectedNotification] = useState(null);
    const [show, setShow] = useState(false);

    // console.log(notifications)
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleView = (notification) => {
        setSelectedNotification(notification);
        setShowModal(true);
    };

    const handleAccept = async (requestId) => {
        try {
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            const requestResponse = { response: "accepted" };
            const response = await friendRequestResponse(username, accessToken, requestId, requestResponse);
            if (response.status === 200) {
                if (response.data.success === true) {
                    setShowModal(false);
                    window.location.reload()
                } else {
                    console.log(response.data.message);
                }
            }
        } catch (error) {
            console.log(error);
        }
    };

    const handleReject = async (requestId) => {
        try {
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            const requestResponse = { response: "rejected" };
            const response = await friendRequestResponse(username, accessToken, requestId, requestResponse);
            if (response.status === 200) {
                if (response.data.success === true) {
                    setShowModal(false);
                    window.location.reload()
                } else {
                    console.log(response.data.message);
                }
            }
        } catch (error) {
            console.log(error);
        }
    };

    const handleClear = async (notificationData) => {
        try {
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            // console.log(notificationData)
            const response = await notificationClear(username, accessToken, notificationData);
            if (response.status === 200) {
                if (response.data.success === true) {
                    console.log("notification cleared")
                    window.location.reload()
                } else {
                    console.log(response.data.message);
                }
            }
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <div data-testid="notification-component">
        <div className="notification-container">
            <Button className="btn btn-8" onClick={handleShow}>
                Notifications
            <img src={`${process.env.PUBLIC_URL}/notifications-icon.png`} width="25px" height="25px" />
            
            </Button>
            <Offcanvas className="notifications-offcanvas" show={show} onHide={handleClose}>
                <Offcanvas.Header className="notifications-header" closeButton>
                    <h1>Notifications</h1>
                </Offcanvas.Header>
                <Offcanvas.Body className="notifications-body">
                    <ul>
                        {Array.isArray(notifications) &&
                            notifications.map((notification, index) => (
                                <li className="notifications-li" key={index}>
                                    {notification.length === 1 ? (
                                        <div>
                                        <p className="notifications-text">{notification}</p>
                                        <br />
                                        <Button className="btn btn-7" onClick={() => handleClear(notification)}>
                                                X
                                            </Button>
                                        </div>
                                    ) : (
                                        <>
                                            <p className="notifications-text">Friend request from: {notification[0]}{' '}</p>
                                            <Button className="btn btn-2" onClick={() => handleView(notification)}>
                                                View
                                            </Button>
                                        </>
                                    )}
                                </li>
                            ))}
                    </ul>
                </Offcanvas.Body>
            </Offcanvas>

            <Modal show={showModal} onHide={() => setShowModal(false)} centered>
                <Modal.Body className="notification-modal-body">
                    <p>Friend Request from {selectedNotification?.[0]}</p>
                    <p>Status: {selectedNotification?.[2]}</p>
                    <Button className="btn btn-1" onClick={() => handleAccept(selectedNotification?.[4])}>
                        Accept
                    </Button>
                    <Button className="btn btn-3" onClick={() => handleReject(selectedNotification?.[4])}>
                        Reject
                    </Button>
                    <Button className="btn btn-7" onClick={() => setShowModal(false)}>
                        Cancel
                    </Button>
                </Modal.Body>
            </Modal>
        </div>
        </div>
    );
};

export default Notification;
