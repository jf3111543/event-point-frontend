import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getUserDetails } from '../lib/api/backend-server';
import { CreateListModal, EditListModal, AddCollaboratorModal } from './ListModals';
import { getListDetails } from '../lib/api/backend-server';
import { Button, Modal, ModalHeader } from 'react-bootstrap';
import { getCookie } from '../cookieConfig';
import Header from './Header';
import { removeEvent } from '../lib/api/backend-server';
import { getUserLists } from '../lib/api/backend-server';
import LeaveList from './LeaveList'
import EventInformation from './EventInformation';

function UserProfile() {

    //holds the user data
    const [user, setUser] = useState([]);
    const [lists, setLists] = useState([]);

    //react Router hook for navigation
    const navigate = useNavigate();

    //fetch the list of events when the component mounts
    useEffect(() => {
        fetchUserDetails();
        fetchUserLists();
    }, []);

    const fetchUserDetails = async () => {
        try {
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            if (!!username && !!accessToken) {
                const details = await getUserDetails(username, accessToken);
                if (details.status = 200) {
                    if (details.data.success == true) {
                        // console.log("const detaiils:")
                        console.log(details.data)
                        setUser(details.data.data)
                        console.log(details.data.data)
                        // return details
                    }
                    else {
                        console.log(details.data.message)
                    }
                }
            }
            else {
                console.log("User is not logged in");
                navigate('/')

            }
        } catch (error) {
            console.log(error);
        }
    };

    const fetchUserLists = async () => {
        try {
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            if (!!username && !!accessToken) {
                const details = await getUserLists(username, accessToken);
                if (details.status = 200) {
                    if (details.data.success == true) {
                        // console.log("const detaiils:")
                        console.log(details.data)
                        setLists(details.data.data)
                        console.log(details.data.data)
                        // return details
                    }
                    else {
                        console.log(details.data.message)
                    }
                }
            }
            else {
                console.log("User is not logged in");
                navigate('/')

            }
        } catch (error) {
            console.log(error);
        }
    };

    //holds the list data
    const [list, setList] = useState([]);

    const [selectedListId, setSelectedListId] = useState(null);

    //get the data of selected list
    const fetchListDetails = async (listId) => {
        try {
            try {
                console.log(listId)
                const username = getCookie('username')
                const accessToken = getCookie('access_token')
                const response = await getListDetails(username, accessToken, listId);

                if (response.status == 200) {
                    if (response.data.success == true) {
                        // console.log("List succesfully found");
                        console.log(response.data.data)
                        setList(response.data.data)
                        setShow(true);
                        console.log(listId)
                        setSelectedListId(listId);
                        // console.log("SelectedListId: ", selectedListId)
                    }
                } else {
                    console.log(response.data.message);
                }
            } catch (error) {
                console.log(error);
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    const [show, setShow] = useState(false);
    const [selectedEventId, setSelectedEventId] = useState(null);
    const [showEventInformation, setShowEventInformation] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleEventInformation = (eventId) => {
        // console.log("the selected event ID when the button is clicked:")
        // console.log(eventId)
        //check eventId is not undefined
        if (eventId !== undefined) {
          setSelectedEventId(eventId);
          setShowEventInformation(true);
        }
      };

      
      
      const handleRemoveEvent = async (listId, eventId) => {
        try {
            console.log("handle remove function called")
            const username = getCookie('username')
            const accessToken = getCookie('access_token')

            const response = await removeEvent(username, accessToken, listId, eventId)
            if (response.status == 200) {
                if (response.data.success == true) {
                    console.log("Event removed");
                    window.location.reload()
                }
            } else {
                console.log(response.data);
            }

        } catch (error) {
            console.log(error)
        }

    }

    const confirmRemove = (listId, eventId) => {
        const isConfirmed = window.confirm("Are you sure you want to remove this event?");
        if (isConfirmed) {
            handleRemoveEvent(listId, eventId);
        }
    }


    return (
        <div data-testid="user-profile-component">
            <div className="container-fluid justify-content-center align-items-center" id="wrapper">

                <Header />

                <div className="container-fluid">
                <div className="row align-items-center main-content">
                    <div className="row user-profile-header">
                        <div className="col profile-img-col">
                            <img src={`${process.env.PUBLIC_URL}/logo-dark.jpg`} alt="profile picture placeholder" width="200" height="200" />
                        </div>
                        <div className="col profile-information-col">
                            <h1> {user.username} </h1>
                            <p> {user.profile_visible ? 'Public' : 'Private'} </p>
                            <button className="btn btn-5" onClick={() => navigate('/account/edit')}> Edit </button>
                        </div>
                    </div>

                    <div className="row friend-list-row">
                        <div className="col list-col">
                            <div className="list-title">
                            <h2> Lists </h2>
                            </div>
                            <div className="list-text">
                            <p> Number of Lists: {user.numberLists > 0 ? user.numberLists : 'No lists found'} </p>
                            </div>
                            <ul className="list-ul">

                                {user && lists && lists.map((list, index) => (
                                    <li className="list-li" key={index} onClick={() => fetchListDetails(list.id)} >
                                        <p className="list-li-text" onClick={handleShow}>
                                            {list.name}
                                        </p>
                                    </li>
                                ))}
                                <Modal show={show} onHide={handleClose} centered>
                                    <ModalHeader className="list-modal-header">
                                        <h1> {list.name} </h1>
                                    </ModalHeader>
                                    <Modal.Body className="list-modal-body">
                                        <p> {list.visibility ? 'Public' : 'Private'}</p>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    {list.collaborators && list.collaborators.length > 0 ? (    
                                                        list.collaborators.map((collaborator, index) => (
                                                            <td key={index}>
                                                                {collaborator.username !== undefined ? collaborator.username : 'Username not found'}
                                                            </td>
                                                        ))
                                                    ) : (
                                                        <></>
                                                    )}
                                                </tr>
                                            </tbody>
                                        </table>
                                        <br />
                                        <br />
                                        <h4> Events: </h4>
                                        <ul className="list-ul">
                                            {list.events && list.events.length > 0 ? (    
                                                list.events.map((event, index) => (
                                                    <li className="list-event-li" key={index}>
                                                        <p className="list-event-li-text">{event.eventName !== undefined ? event.eventName : 'No Event found'}</p>
                                                        <button className="btn btn-6" onClick={() => handleEventInformation(event.eventId)}>View</button>
                                                        {/* <button className="btn btn-6" variant="primary" onClick={() => handleViewEvent(event.eventId)}>View</button> */}
                                                        <button className="btn btn-3" onClick={() => confirmRemove(selectedListId, event.eventId)}>Remove</button>
      
                                                    </li>
                                                ))
                                            ) : (
                                                <li>No Events found</li>
                                            )}
                                        </ul>
                                    </Modal.Body>
                                    <Modal.Footer className="list-modal-header">
                                        {/* { console.log(selectedListId)} */}
                                        <AddCollaboratorModal listId={selectedListId} />
                                        {/* Check if there are other collaborators in the list before giving the option to leave */}
                                        {list.collaborators && list.collaborators.length > 1 ? (    
                                            <LeaveList listId={selectedListId} />
                                        ) : (
                                            <></>
                                        )}
                                        <EditListModal listId={selectedListId} />
                                        <Button className="btn btn-7" onClick={handleClose}>
                                            Close
                                        </Button>
                                    </Modal.Footer>
                                </Modal>

                                <li className="btn-li">
                                    <CreateListModal />
                                </li>

                            </ul>
                            
                        </div>

                        <div className="col friend-col">
                            <div className="friend-title">
                            <h2> Friends</h2>
                            </div>
                            <div className="friend-text">
                            <p> Number of Friends: {user.numberFriends > 0 ? user.numberFriends : 'No friends found'} </p>
                            </div>
                            <ul className="friend-ul">
                                {user && user.friends && user.friends.map((friend, index) => (
                                    <li className="btn friend-li" key={index}>
                                       <p className="friend-li-text" onClick={() => navigate(`/user/profile/${friend.friendUsername}`)}>{friend.friendUsername} </p>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        

                    </div>
                    
                </div>
                </div>
                {showEventInformation && (
                    <EventInformation show={showEventInformation} handleClose={() => setShowEventInformation(false)} eventId={selectedEventId} />
                  )}

                <div className="row align-items-center footer">
                    <span>&copy; EventPoint</span>
                </div>

            </div>
        </div>
    );
};



export default UserProfile;