import { useState } from 'react';
import { addUser } from '../lib/api/backend-server';
import { useNavigate, Link } from 'react-router-dom';
import { loginUser } from '../lib/api/backend-server';
import { setCookie, getCookie, removeCookie } from '../cookieConfig';
import Header from './Header';
import { Carousel } from 'react-bootstrap';
import validator from 'validator';

function Signup() {

  const navigate = useNavigate();

  const [value, setValue] = useState({});
  const [error, setError] = useState({});
  
  //function to handle changes in input fields
  const handleChange = (e) => {
    const { name, value } = e.target;

    //update the state with the new value for the corresponding input field
    setValue((prevValue) => ({
      ...prevValue,
      // ...defaultValues,
      [name]: value !== null ? value : '',
    }));
  };

  const validateInput = () => {
    let errors = {};

    if (!validator.isLength(value.username || '', { min: 3, max: 64})) {
      errors.username = 'Username must be between 3-64 characters long';
    }
  
    if (!validator.isEmail(value.email || '')) {
      errors.email = 'Please enter a valid email address';
    }
  
    if (!validator.isLength(value.password || '', { min: 6, max: 120})) {
      errors.password = 'Password must be at least 6 characters long';
    } else if (!/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]/.test(value.password)) {
      errors.password = 'Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character';
    }
  
    setError(errors);
    return Object.keys(errors).length === 0;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!validateInput()) {
      return;
    }

    try {
      console.log(value);
      await addUser(value).then(response => {
        console.log(response.data)
        if (response.status = 200) {
          if (response.data.success == true) {
            console.log(response.data.message)
            let loginDetails = {
              email: value.email,
              password: value.password
            };

            loginUser(loginDetails).then(response => {
              console.log("Response after logging in")
              console.log(response)
              if (response.status = 200) {
                //console.log(response.data.success)
                if (response.data.success == true) {
                  setCookie('access_token', response.data.accessToken)
                  setCookie('username', response.data.data.username)
                  // console.log(response.data.accessToken)
                  // console.log(response.data.data.username)
                  console.log("Access Token and Username succesfully stored")
                  navigate('/')

                  //window.location.href = '/';
                }
              }
            });
          }
          else {
            console.log(response.data.message)
            setError({error: response.data.message})
          }
        }
        // console.log(response)
        // navigate('/login')
      });
    } catch (error) {
      console.log(error.response.data.error);
    }
  };

  return (
    <div data-testid="signup-component">
      <div className="container-fluid justify-content-center align-items-center" id="wrapper">

        <Header />
        <div className="container-fluid">
          <div className="row align-items-center main-content">
            <div className="row signup-title">
              <h1> Create an Account</h1>
            </div>
            <div className="row form-row">
              <div className="col-sm-6">
                <div className="row">
                  <form>
                    <div>
                      <label htmlFor="username"> Username: </label>
                      <input className="form-control account-form-input" type="text" placeholder="Enter username" name="username" onChange={handleChange} value={value.username || ''} required />
                      {error.username && <span style={{ color: 'red' }}>{error.username}</span>}

                      <br />

                      <label htmlFor="email"> Email: </label>
                      <input className="form-control account-form-input" type="email" placeholder="Enter email" name="email" onChange={handleChange} value={value.email || ''} required />
                      {error.email && <span style={{ color: 'red' }}>{error.email}</span>}

                      <br />

                      <label htmlFor="password"> Password: </label>
                      <input className="form-control account-form-input" type="password" placeholder="Enter password" name="password" onChange={handleChange} value={value.password || ''} required />
                      {error.password && <span style={{ color: 'red' }}>{error.password}</span>}
                    </div>
                    {error.error && <span style={{ color: 'red' }}>{error.error}</span>}
                    <div>
                      <button type="submit" className="btn btn-2" onClick={handleSubmit}>Create Account</button>
                      
                    </div>
                  </form>
                </div>
                <div className="row alternative-row">
                  <p className="alternative-text">Already have an Account?</p>
                  <Link to={`/login`} className="btn btn-4"> Login </Link>
                </div>
              </div>
              <div className="col-sm-6">
                <div className="row account-carousel-row">
                  <div className="account-carousel">
                    <Carousel>
                      <Carousel.Item>
                        <div className="carousel-img">
                          <img className="image-carousel" src={`${process.env.PUBLIC_URL}/default-event-img.jpg`} alt="image" />
                        </div>
                        <Carousel.Caption>
                          <h3> Discover Events</h3>

                        </Carousel.Caption>
                      </Carousel.Item>
                      <Carousel.Item>
                        <div className="carousel-img">
                          <img className="image-carousel" src={`${process.env.PUBLIC_URL}/logo-light.jpg`} alt="image" />
                        </div>
                        <Carousel.Caption>
                          <h3> Add Friends</h3>

                        </Carousel.Caption>
                      </Carousel.Item>
                      <Carousel.Item>
                        <div className="carousel-img">
                          <img className="image-carousel" src={`${process.env.PUBLIC_URL}/default-event-img.jpg`} alt="image" />
                        </div>
                        <Carousel.Caption>
                          <h3> Create Lists</h3>

                        </Carousel.Caption>
                      </Carousel.Item>

                    </Carousel>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Signup;