import { useState } from 'react';
import { loginUser } from '../lib/api/backend-server';
import { useNavigate, Link } from 'react-router-dom';
import { setCookie, getCookie, removeCookie } from '../cookieConfig';
import Header from './Header';
import EventCarousel from './EventCarousel';
import { Carousel } from 'react-bootstrap';
import validator from 'validator';

function Login() {

    const navigate = useNavigate();

    const [value, setValue] = useState({});
    const [error, setError] = useState({});

    // Function to handle changes in input fields
    const handleChange = (e) => {
        const { name, value } = e.target;
        // Update the state with the new value for the corresponding input field
        setValue((prevValue) => ({
            ...prevValue,
            [name]: value !== null ? value : '',
        }));
    };

    const validateInput = () => {
        let errors = {};
      
        if (!validator.isEmail(value.email || '')) {
          errors.email = 'Please enter a valid email address';
        }
      
        if (!validator.isLength(value.password || '', { min: 6, max: 120})) {
          errors.password = 'Password must be at least 6 characters long';
        }
      
        setError(errors);
        return Object.keys(errors).length === 0;
      };

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!validateInput()) {
            return;
          }

        try {
            //console.log(value);
            await loginUser(value).then(response => {
                console.log("Response after logging in")
                console.log(response)
                if (response.status = 200) {
                    //console.log(response.data.success)
                    if (response.data.success == true) {
                        setCookie('access_token', response.data.accessToken)
                        setCookie('username', response.data.data.username)
                        // console.log(response.data.accessToken)
                        // console.log(response.data.data.username)
                        console.log("Access Token and Username succesfully stored")

                        navigate('/')
                        //window.location.href = '/';
                    }
                    else {
                        console.log(response.data.message)
                        setError({error: response.data.message})
                    }

                }
            });
        } catch (error) {
            console.log(error.response.data.error);
            //console.log("Authorization error")
        }
    };

    return (
        <div data-testid="login-component">
            <div className="container-fluid justify-content-center align-items-center" id="wrapper">

                <Header />
                <div className="container-fluid">
                    <div className="row align-items-center main-content">

                        <div className="row login-title">
                            <h1> Login </h1>
                        </div>
                        <div className="row form-row">
                            <div className="col-sm-6">
                                <div className="row">
                                    <form>
                                        <div>
                                            <label htmlFor="email"> Email: </label>
                                            <input className="form-control account-form-input" type="email" placeholder="Enter email" name="email" onChange={handleChange} value={value.email || ''} required />
                                            {error.email && <span style={{ color: 'red' }}>{error.email}</span>}

                                            <br />

                                            <label htmlFor="password"> Password: </label>
                                            <input className="form-control account-form-input" type="password" placeholder="Enter password" name="password" onChange={handleChange} value={value.password || ''} required />
                                            {error.password && <span style={{ color: 'red' }}>{error.password}</span>}
                                        </div>
                                        {error.error && <span style={{ color: 'red' }}>{error.error}</span>}
                                        <div>
                                            <button type="submit" className="btn btn-2" onClick={handleSubmit}>Login</button>
                                        </div>
                                    </form>
                                </div>
                                <div className="row alternative-row">
                                    <p className="alternative-text">Don't have an Account?</p>
                                    <Link to={`/signup`} className="btn btn-4"> Signup </Link>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="row account-carousel-row">
                                    <div className="account-carousel">
                                        <Carousel>
                                            <Carousel.Item>
                                                <div className="carousel-img">
                                                    <img className="image-carousel" src={`${process.env.PUBLIC_URL}/default-event-img.jpg`} alt="image" />
                                                </div>
                                                <Carousel.Caption>
                                                    <h3> Discover Events</h3>

                                                </Carousel.Caption>
                                            </Carousel.Item>
                                            <Carousel.Item>
                                                <div className="carousel-img">
                                                    <img className="image-carousel" src={`${process.env.PUBLIC_URL}/logo-light.jpg`} alt="image" />
                                                </div>
                                                <Carousel.Caption>
                                                    <h3> Add Friends</h3>

                                                </Carousel.Caption>
                                            </Carousel.Item>
                                            <Carousel.Item>
                                                <div className="carousel-img">
                                                    <img className="image-carousel" src={`${process.env.PUBLIC_URL}/default-event-img.jpg`} alt="image" />
                                                </div>
                                                <Carousel.Caption>
                                                    <h3> Create Lists</h3>

                                                </Carousel.Caption>
                                            </Carousel.Item>

                                        </Carousel>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;