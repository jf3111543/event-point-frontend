import React from 'react';
import { leaveCustomList } from '../lib/api/backend-server';
import { getCookie } from '../cookieConfig';

function LeaveList({listId}) {

    const handleLeave = async () => {
        try {
            console.log("handle leave function called")
            const username = getCookie('username')
            const accessToken = getCookie('access_token')

            const response = await leaveCustomList(username, accessToken, listId)
            if (response.status == 200) {
                if (response.data.success == true) {
                    console.log("User Left List");
                    console.log(response.data.data)
                    window.location.reload()
                }
            } else {
                console.log(response.data.message);
            }

        } catch (error) {
            console.log(error)
        }

    }

    const confirmLeave = () => {
        const isConfirmed = window.confirm("Are you sure you want to leave this list?");
        if (isConfirmed) {
            handleLeave();
        }
    }


    return (
        <div>
            <button className="btn btn-3" onClick={confirmLeave} >Leave List</button>
        </div>
    );
};

export default LeaveList;