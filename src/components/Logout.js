import React from 'react';
import { logoutUser } from '../lib/api/backend-server';
import { setCookie, getCookie, removeCookie } from '../cookieConfig';

function Logout() {

  const handleLogout = async () => {
    try {
      const username = getCookie('username')
      const accessToken = getCookie('access_token')
      if (!username && !accessToken) {
        console.log("User is not logged in")
      }
      else {
        const response = await logoutUser(username, accessToken)
        if (response.status == 200) {
          if (response.data.success == true) {
            //if user succesfully logs out in the backend server
            //remove authentication token from frontend cookies
            removeCookie('access_token');
            removeCookie('username');

            console.log(response)
            console.log("User logged out");
            //redirect user to login page
            window.location.href = '/';
          }
        } else {
          console.log(response.data.message);
        }
      }

    } catch (error) {
      console.log(error)
    }

  }

  return (
    <div className="logout-btn-div">
      <button className="btn logout-btn" onClick={handleLogout} >Logout</button>
    </div>
  );
};

export default Logout;