import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getUserDetails } from '../lib/api/backend-server';
import { getCookie } from '../cookieConfig';
import Header from './Header';

function UserAccount() {

  //holds the user data
  const [user, setUser] = useState([]);

  //react Router hook for navigation
  const navigate = useNavigate();

  //fetch the list of events when the component mounts
  useEffect(() => {
    fetchUserDetails();
  }, []);

  const fetchUserDetails = async () => {
    try {
      const username = getCookie('username');
      console.log(username)
      const accessToken = getCookie('access_token');
      if (!!username && !!accessToken) {
        const details = await getUserDetails(username, accessToken);
        if (details.status = 200) {
          if (details.data.success == true) {
            // console.log("const detaiils:")
            console.log(details.data)
            setUser(details.data.data)
            console.log(details.data.data)
            // return details
          }
          else {
            console.log(details.data.message)
          }
        }
      }
      else {
        console.log("User is not logged in");
        navigate('/')

      }
    } catch (error) {
      console.log(error);
    }
  };


  return (
    <div data-testid="user-account-component">
      <div className="container-fluid justify-content-center align-items-center" id="wrapper">

        <Header />
        <div className="container-fluid">

          <div className="row align-items-center main-content">
            <div className="row user-account-row">
              <div className="user-account-container">
                <div className="user-account-title">
                  <h3> User Account </h3>
                </div>
                <div className="user-account-information">
                  <p> Username: {user.username} </p>
                  <p> Email: {user.email} </p>
                  <p> Profile visibility: {user.profileVisible ? 'Public' : 'Private'} </p>
                  <p> Number of Lists: {user.numberLists > 0 ? user.numberLists : 'No lists found'} </p>
                  <p> Number of Friends: {user.numberFriends > 0 ? user.numberFriends : 'No friends found'} </p>
                  <div className="row user-account-btn">
                    <button className="btn-1" onClick={() => navigate('/account/edit')}> Edit </button>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div className="row align-items-center footer">
          <span>&copy; EventPoint</span>
        </div>

      </div>
    </div>
  );
};



export default UserAccount;