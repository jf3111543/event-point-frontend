import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getEvents } from '../lib/api/backend-server';
import AddToList from './AddToList';
import { getCookie } from '../cookieConfig';
import Header from './Header';
import EventInformation from './EventInformation';
import { Pagination } from 'react-bootstrap';
import EventCarousel from './EventCarousel';

function ViewEvents() {
  //holds the list of events
  const [events, setEvents] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [eventsPerPage] = useState(24);
  //react Router hook for navigation
  const navigate = useNavigate();

  //fetch the list of events when the component mounts
  useEffect(() => {
    fetchEvents();
  }, []);

  //function to fetch the list of eventa from the API
  const fetchEvents = async () => {
    try {
      const response = await getEvents();
      //console.log(response.data)
      setEvents(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const [searchTerm, setSearchTerm] = useState('');
  const [selectedVenue, setSelectedVenue] = useState('');
  const [selectedDate, setSelectedDate] = useState('');
  const [filteredData, setFilteredData] = useState([]);

  const handleInputChange = (e) => {
    const { value } = e.target;
    setSearchTerm(value);
    filterEvents(value, selectedVenue, selectedDate);
  };

  const handleVenueChange = (e) => {
    const { value } = e.target;
    setSelectedVenue(value);
    filterEvents(searchTerm, value, selectedDate);
  };

  const handleDateChange = (e) => {
    const { value } = e.target;
    setSelectedDate(value);
    filterEvents(searchTerm, selectedVenue, value);
  };

  const filterEvents = (searchTerm, venue, date) => {

    // console.log('searchTerm:', searchTerm);
    // console.log('venue:', venue);
    // console.log('date:', date);

    // console.log('Initial events:', events);

    //create a copy ather than edit the original
    let filteredData = [...events];

    if (searchTerm) {
      //console.log("Filtering by searchTerm:"", searchTerm);
      filteredData = filteredData.filter((item) =>
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    if (venue) {
      // console.log("Filtering by venue:", venue);
      filteredData = filteredData.filter((item) => item.venue[0].name.toLowerCase() === venue.toLowerCase());
    }

    if (date) {
      // console.log("Filtering by date:", date);
      //filteredData = filteredData.filter((item) => item.dates.start.dateTime.toLowerCase() === date.toLowerCase());
      filteredData = filteredData.filter((item) => {
        // console.log(item.dates?.start?.dateTime)
        
        const dateTime = item.dates?.start?.dateTime;
        if (dateTime) {
            const dateString = String(dateTime).slice(0, 10);
            const comparisonDate = String(date).slice(0, 10);
            return dateString === comparisonDate;
        }
        return false;
    });
    }

    // console.log('Filtered events:', filteredData);

    setFilteredData(filteredData);
  };

//fetch the login status when the component mounts
  useEffect(() => {
    checkLoginStatus();
  }, []);

  const [loggedIn, setLoggedIn] = useState('');
  const checkLoginStatus = () => {
    try {
      const username = getCookie('username')
      const accessToken = getCookie('access_token')
      if (!username && !accessToken) {
        // console.log("User is not logged in")
        setLoggedIn(false)
      }
      else {
        // console.log("User is logged in")
        setLoggedIn(true)
        
      }
      
    } catch (error) {
      console.log(error)
    }

  }

  const [selectedEventId, setSelectedEventId] = useState(null);
  const [showAddToList, setShowAddToList] = useState(false);
  const [showEventInformation, setShowEventInformation] = useState(false);

  const handleEventSave = (eventId) => {
    if (loggedIn){
      // console.log(eventId)
      //check eventId is not undefined
      if (eventId !== undefined) {
        setSelectedEventId(eventId)
          setShowAddToList(true);
      }
    } else {
        // console.log("User is not logged in")
        window.alert('You need to be logged in to perform this action')
    } 
  };

  const handleEventInformation = (eventId) => {
    // console.log("the selected event ID when the button is clicked:")
    // console.log(eventId)
    //check eventId is not undefined
    if (eventId !== undefined) {
      setSelectedEventId(eventId);
      setShowEventInformation(true);
    }
  };

  const indexOfLastEvent = currentPage * eventsPerPage;
  const indexOfFirstEvent = indexOfLastEvent - eventsPerPage;
  // console.log(indexOfFirstEvent)
  // console.log(indexOfLastEvent)
  const currentEvents = events.slice(indexOfFirstEvent, indexOfLastEvent);
  // console.log(currentEvents)

  const paginate = (pageNumber) => setCurrentPage(pageNumber);


  return (
    <div data-testid="view-events-component">
      <div className="container-fluid justify-content-center align-items-center" id="wrapper">
        
        <Header />
        <div className="container-fluid">
        <div className="row align-items-center main-content">

          <div className="row carousel-row">
              <div className="col-sm-6 carousel-col">
                <div className="carousel-container">
                <EventCarousel />
                </div>
              </div>
              <div className="col-sm-6">
                <div className="greeting-text">
                  <p>Welcome to EventPoint</p>
                  <h2>Browse and Discover Events</h2>
                  <br />
                  <h3>Add Friends and Create Lists</h3>
                
                </div>
              </div>
            
          </div>
          <br />
          <div className="row search-filter-row">

            <div className='col-md search-filter-col'>
              <input className="search-input" id="searchInput" type="text" placeholder="Search Events..." value={searchTerm} onChange={handleInputChange} />
            </div>

            <div className='col-md dropdown-col'>
              {/* <div className='row'> */}
              {/* <div className='filter'> */}

                <div className='dropdown-filter'>
                <select className="dropdown-box" id="dateDropdown" value={selectedVenue} onChange={handleVenueChange}>
                  <option className='dropdown-option' value="">Select Venue</option>
                  {[...new Set(events.map(item => item.venue[0].name))].sort().map(venueName => (
                    <option className='dropdown-option' key={venueName} value={venueName} data-testid={venueName}>
                      {venueName}
                    </option>
                  ))}
                </select>
                </div>
              {/* </div> */}

                <div className='dropdown-filter'>
                <select className="dropdown-box" id="dateVenue" value={selectedDate} onChange={handleDateChange}>
                  <option className='dropdown-option' value="">Select Date</option>
                  {([...new Set(events.map(item => {
                    const date = item.dates?.start?.dateTime;
                    if (date !== undefined) {
                      return typeof date === 'string' ? date.slice(0, 10) : String(date).slice(0, 10);
                    }
                    return null; //return null undefined dates
                  }))].filter(date => date !== null) //remove null values from options
                  .sort().map((date, index) => (
                    <option className='dropdown-option' key={index} value={date} data-testid={date}>
                      {date}
                    </option>
                  )))}
                </select>
                </div>
                
              {/* </div> */}

            </div>

          </div>



          <div className="row event-listings-row">
            {currentEvents.length > 0 ? (
              <div className="card-deck">

                {/* ternary operator to check conditions of search input  */}
                {(!searchTerm && !selectedVenue && !selectedDate) ? (
                  //display full list if no search input is present
                  currentEvents.map((item) => (
                    <div className="card" style={{ width: '18rem' }} key={item.id}>
                      <div className="card-body">
                        <div className="row">

                          <h4 className="card-title">{item.name}</h4>
                          <p> {item.venue[0].name} </p>
                          <p> {item.dates?.start?.dateTime ? item.dates.start.dateTime.slice(0, 10) : String(item.dates?.start?.dateTime).slice(0, 10)} </p>
                          <div className="card-img-container">
                            <div className="card-img">
                              <img src={item.images?.url || `${process.env.PUBLIC_URL}/default-event-img.jpg`} alt="the event" />
                            </div>
                          </div>
                        </div>
                        <div className="row card-btn-row">
                          <button className="btn btn-4" onClick={() => handleEventSave(item.id)} >
                            Save to List
                          </button>
                          <button className="btn btn-2" onClick={() => handleEventInformation(item.id)}> View Details</button>
                        </div>
                      </div>
                    </div>
                  ))
                ) : filteredData.length > 0 ? (
                  //if there is a result based on search input, display the filtered data
                  filteredData.map((item) => (
                    <div className="card" style={{ width: '18rem' }} key={item.id}>
                      <div className="card-body">
                        <div className="row">

                          <h4 className="card-title">{item.name}</h4>
                          <p> {item.venue[0].name} </p>
                          <p> {item.dates?.start?.dateTime ? item.dates.start.dateTime.slice(0, 10) : String(item.dates?.start?.dateTime).slice(0, 10)} </p>
                          <div className="card-img-container">
                          <div className="card-img">
                            <img src={item.images?.url || `${process.env.PUBLIC_URL}/default-event-img.jpg`} alt="the event" /> 
                          </div>
                        </div>
                        </div>
                        <div className="row card-btn-row">

                          <button className="btn btn-4" onClick={() => handleEventSave(item.id)} >
                            Save to List
                          </button>

                          <button className="btn btn-2" onClick={() => handleEventInformation(item.id)}> View Details</button>
                        </div>
                      </div>
                    </div>
                  ))
                ) : (searchTerm && filteredData.length === 0) && (
                  //if there is a search input but no results found
                  <p>No results found</p>
                )}
              </div>
            ) : (
              <p> No events found </p>
            )}
            <div className="row pagnation-row">
            <Pagination className="custom-pagnation">
              <Pagination.Prev id="pagnation-link" onClick={() => paginate(currentPage - 1)} disabled={currentPage === 1} />
              {Array.from(
                {
                  length: Math.ceil(
                    filteredData.length > 0 ? filteredData.length / eventsPerPage : events.length / eventsPerPage
                  )
                },
                (_, index) => (
                  <Pagination.Item id="pagnation-link" key={index} active={index + 1 === currentPage} onClick={() => paginate(index + 1)} >
                    {index + 1}
                  </Pagination.Item>
                )
              )}
              <Pagination.Next id="pagnation-link" onClick={() => paginate(currentPage + 1)} disabled={currentPage === Math.ceil( filteredData.length > 0 ? filteredData.length / eventsPerPage : events.length / eventsPerPage )} />
            </Pagination>
            </div>
          </div>
          {/* pass event id to modal componenet */}
          {/* <AddToList show={showAddToList} handleClose={() => setShowAddToList(false)} eventId={selectedEventId} />
          <EventInformation show={showEventInformation} handleClose={() => setShowEventInformation(false)} eventId={selectedEventId} /> */}
            
            {showAddToList && (
                    <AddToList show={showAddToList} handleClose={() => setShowAddToList(false)} eventId={selectedEventId} />
                  )}
                  {showEventInformation && (
                    <EventInformation show={showEventInformation} handleClose={() => setShowEventInformation(false)} eventId={selectedEventId} />
                  )}

        </div>
        </div>

        <div className="row align-items-center footer">
          <span>&copy; EventPoint</span>
        </div>

      </div>
    </div>
  );

};



export default ViewEvents;