import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getUserDetails, editUserDetails } from '../lib/api/backend-server';
import { setCookie, getCookie } from '../cookieConfig';
import DeleteUser from './DeleteUser';
import Header from './Header';
import { Carousel } from 'react-bootstrap';
import validator from 'validator';

function EditUser() {

    //holds the user data
    const [user, setUser] = useState({
        username: '',
        email: '',
        password: '',
        profileVisible: false
    });

    const [error, setError] = useState({});

    //react Router hook for navigation
    const navigate = useNavigate();

    //fetch the list of events when the component mounts
    useEffect(() => {
        fetchUserDetails();
    }, []);

    const fetchUserDetails = async () => {
        try {
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            if (!!username && !!accessToken) {
                const details = await getUserDetails(username, accessToken);
                if (details.status = 200) {
                    if (details.data.success == true) {
                        // console.log("const detaiils:")
                        console.log(details.data)
                        setUser(details.data.data)
                        console.log(details.data.data)
                        // return details
                    }
                    else {
                        console.log(details.data.message)
                    }
                }
            }
            else {
                console.log("User is not logged in");
                navigate('/')

            }
        } catch (error) {
            console.log(error);
        }
    };


    const handleChange = (e) => {
        const { name, type, checked } = e.target;
        const inputValue = type === 'checkbox' ? checked : e.target.value;

        setUser((prevData) => ({
            ...prevData,
            [name]: inputValue,
        }));

        console.log("User state after handleChange:", user);
    };

    const validateInput = () => {
        let errors = {};
    
        if (!validator.isLength(user.username, { min: 3, max: 64})) {
          errors.username = 'Username must be between 3-64 characters long';
        }
      
        if (!validator.isEmail(user.email)) {
          errors.email = 'Please enter a valid email address';
        }
      
        if (!validator.isLength(user.password, { min: 6, max: 120})) {
          errors.password = 'Password must be at least 6 characters long';
        }
      
        setError(errors);
        return Object.keys(errors).length === 0;
      };

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!validateInput()) {
            return;
        }

        try {
            console.log("editUser component called")
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            // console.log(list_id)
            // console.log(formData)
            if (!!username && !!accessToken) {
                const response = await editUserDetails(username, accessToken, user);
                if (response.status = 200) {
                    if (response.data.success == true) {
                        //updating th =e access token and username cookies to new details
                        if (!!response.data.data.username) {
                            setCookie('username', response.data.data.username)
                            console.log(getCookie('username'))
                        }
                        if (!!response.data.accessToken) {
                            setCookie('access_token', response.data.accessToken)
                            console.log(getCookie('access_token'))
                        }
                       navigate('/account')
                    }
                }
                else {
                    console.log(response.data.message)
                    setError({error: response.data.message})
                }
            }
        } catch (error) {
            console.error(error);
        }
    };


    return (
        <div data-testid="edit-user-component">
            <div className="container-fluid justify-content-center align-items-center" id="wrapper">

                <Header />
                <div className="container-fluid">
                    <div className="row align-items-center main-content">
                        <div className="row edit-account-title">
                            <h1> Edit Account </h1>
                        </div>
                        <div className="row form-row">
                            <div className="col-sm-6">
                                <div className="row">
                                    <form>
                                        <div>
                                            <label htmlFor="username"> Username: </label>
                                            <input className="form-control account-form-input" type="text" placeholder="Enter username" name="username" onChange={handleChange} value={user.username} required />
                                            {error.username && <span style={{ color: 'red' }}>{error.username}</span>}

                                            <br />

                                            <label htmlFor="email"> Email: </label>
                                            <input className="form-control account-form-input" type="email" placeholder="Enter email" name="email" onChange={handleChange} value={user.email} required />
                                            {error.email && <span style={{ color: 'red' }}>{error.email}</span>}

                                            <br />

                                            <label htmlFor="password" style={{ display: 'none' }}> Password: </label>
                                            <input className="form-control account-form-input" type="password" placeholder="Enter password" name="password" onChange={handleChange} value={user.password} required style={{ display: 'none' }} />
                                            {error.password && <span style={{ color: 'red' }}>{error.password}</span>}
                    
                                            <br />
                                            
                                            <label htmlFor="visibility">Public Profile:</label>
                                            {user && (
                                                <input type="checkbox" name="profile_visible" checked={user.profileVisible} onChange={handleChange} />
                                            )}
                                            <br />

                                        </div>
                                        {error.error && <span style={{ color: 'red' }}>{error.error}</span>}
                                        <div>
                                            <button type="submit" className="btn btn-2" onClick={handleSubmit}>Save</button>
                                            <button className="btn btn-7" onClick={() => navigate('/account')}>Cancel</button>
                                        </div>
                                    </form>
                                </div>
                                <div className="row alternative-row">
                                    <DeleteUser />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="row account-carousel-row">
                                    <div className="account-carousel">
                                        <Carousel>
                                            <Carousel.Item>
                                                <div className="carousel-img">
                                                    <img className="image-carousel" src={`${process.env.PUBLIC_URL}/default-event-img.jpg`} alt="image" />
                                                </div>
                                                <Carousel.Caption>
                                                    <h3> Discover Events</h3>

                                                </Carousel.Caption>
                                            </Carousel.Item>
                                            <Carousel.Item>
                                                <div className="carousel-img">
                                                    <img className="image-carousel" src={`${process.env.PUBLIC_URL}/logo-light.jpg`} alt="image" />
                                                </div>
                                                <Carousel.Caption>
                                                    <h3> Add Friends</h3>

                                                </Carousel.Caption>
                                            </Carousel.Item>
                                            <Carousel.Item>
                                                <div className="carousel-img">
                                                    <img className="image-carousel" src={`${process.env.PUBLIC_URL}/default-event-img.jpg`} alt="image" />
                                                </div>
                                                <Carousel.Caption>
                                                    <h3> Create Lists</h3>

                                                </Carousel.Caption>
                                            </Carousel.Item>

                                        </Carousel>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};



export default EditUser;