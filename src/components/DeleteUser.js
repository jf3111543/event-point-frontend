import React from 'react';
import { deleteUserDetails } from '../lib/api/backend-server';
import { getCookie, removeCookie } from '../cookieConfig';

function DeleteUser() {

    const handleDelete = async () => {
        try {
            console.log("handle delete user function called")
            const username = getCookie('username')
            const accessToken = getCookie('access_token')

            const response = await deleteUserDetails(username, accessToken)
            if (response.status == 200) {
                if (response.data.success == true) {
                    console.log("User deleted", response.data);
                    
                    removeCookie('access_token');
                    removeCookie('username');

                    window.location.href = '/';
                }
            } else {
                console.log(response.data);
            }

        } catch (error) {
            console.log(error)
        }

    }

    const confirmDelete = () => {
        const isConfirmed = window.confirm("Are you sure you want to delete your account?");
        if (isConfirmed) {
            handleDelete();
        }
    }

    return (
        <div>
            <button className="btn btn-2" onClick={confirmDelete} >Delete</button>
        </div>
    );
};

export default DeleteUser;