import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getUsersProfiles } from '../lib/api/backend-server';
import { getCookie } from '../cookieConfig';
import Header from './Header';

function SearchUser() {
    //holds the list of users
    const [users, setUsers] = useState([]);
    //react Router hook for navigation
    const navigate = useNavigate();

    //fetch the list of users when the component mounts
    useEffect(() => {
        fetchUsers();
    }, []);

    //function to fetch the list of users from the API
    const fetchUsers = async () => {
        try {
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            if (!!username && !!accessToken) {
                const response = await getUsersProfiles(username, accessToken);
                if (response.status = 200) {
                    if (response.data.success == true) {
                        console.log(response.data.data)
                        setUsers(response.data.data)
                    }
                    else {
                        console.log(response.data.message)
                    }
                }
            }
            else {
                console.log("User is not logged in");
                navigate('/')

            }
        } catch (error) {
            console.error(error);
        }
    };

    const [searchTerm, setSearchTerm] = useState('');
    const [filteredData, setFilteredData] = useState([]);
    const handleInputChange = (e) => {
        const { value } = e.target;
        setSearchTerm(value);
        filterUsers(value, searchTerm);
    };

    const filterUsers = (searchTerm) => {
        // console.log('searchTerm:', searchTerm);
        // console.log('Initial users:', users);

        //create a copy ather than edit the original
        let filteredData = [...users];

        if (searchTerm) {
            //console.log("Filtering by searchTerm:"", searchTerm);
            filteredData = filteredData.filter((item) =>
                item.username.toLowerCase().includes(searchTerm.toLowerCase())
            );
        }
        // console.log('Filtered users:', filteredData);

        setFilteredData(filteredData);
    };

    return (
        <div data-testid="search-user-component">
            <div className="container-fluid justify-content-center align-items-center" id="wrapper">

                <Header />

                <div className="row align-items-center main-content">
                    <div className="row user-search-filter">

                        <div className='col-md search-filter-col'>
                            <input className="search-input users-search-input" id="searchInput" type="text" placeholder="Search Users..." value={searchTerm} onChange={handleInputChange} />
                        </div>


                    </div>



                    <div className="row users-row">
                        {users.length > 0 ? (
                            <div className="card-deck user-card-deck">

                                {/* ternary operator to check conditions of search input  */}
                                {(!searchTerm) ? (
                                    //display full list if no search input is present
                                    users.map((item) => (
                                        <div className="card" style={{ width: '18rem' }} key={item.username}>
                                            <div className="card-body user-card-body">
                                                <div className="row">

                                                    <h5 className="card-text">{item.username}</h5>

                                                </div>
                                                <div className="row">
                                                    <button className="btn btn-4" onClick={() => navigate(`/user/profile/${item.username}`)}>View Profile </button>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                ) : filteredData.length > 0 ? (
                                    //if there is a result based on search input, display the filtered data
                                    filteredData.map((item) => (
                                        <div className="card" style={{ width: '18rem' }} key={item.username}>
                                            <div className="card-body user-card-body">
                                                <div className="row">

                                                    <h5 className="card-text">{item.username}</h5>

                                                </div>
                                                <div className="row">
                                                    <button className="btn btn-4" onClick={() => navigate(`/user/profile/${item.username}`)}>View Profile </button>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                ) : (searchTerm && filteredData.length === 0) && (
                                    //if there is a search input but no results found
                                    <p>No results found</p>
                                )}
                            </div>
                        ) : (
                            <p> No users found </p>
                        )}
                    </div>
                </div>

                <div className="row align-items-center footer">
                    <span>&copy; EventPoint</span>
                </div>

            </div>
        </div>
    );

};



export default SearchUser;