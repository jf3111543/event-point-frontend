import React from 'react';
import { getCookie } from '../cookieConfig';
import { removeFriend } from '../lib/api/backend-server';

function RemoveFriend({ selected_username }) {

    const handleRemove = async () => {
        try {
            console.log("handle remove function called")
            const username = getCookie('username')
            const accessToken = getCookie('access_token')

            console.log(selected_username)
            if (!!selected_username) { 
                const response = await removeFriend(username, accessToken, selected_username)
                // console.log("response", response)
                if (response.status == 200) {
                    if (response.data.success == true) {
                        console.log("Friend Removed");
                        console.log(response.data.data)
                        window.location.reload()
                    }
                } else {
                    console.log(response.data.message);
                }
            }


        } catch (error) {
            console.log(error)
        }

    }

    const confirmRemove = () => {
        const isConfirmed = window.confirm("Are you sure you want to remove this friend? This action will not affect your lists.");
        if (isConfirmed) {
            handleRemove();
        }
    }




    return (
        <div>
            <button className="btn btn-5" onClick={confirmRemove} >Remove Friend</button>
        </div>
    );
};

export default RemoveFriend;