import { useEffect, useState } from 'react';
import { Button, Modal, ListGroup } from 'react-bootstrap';
import { CreateListModal } from './ListModals';
import { useNavigate } from 'react-router-dom';
import { getUserDetails } from '../lib/api/backend-server';
import { addEventToList } from '../lib/api/backend-server';
import { getCookie } from '../cookieConfig';
import { getUserLists } from '../lib/api/backend-server';

function AddToList({ show, handleClose, eventId }) {
  const [user, setUser] = useState([]);
  const [lists, setLists] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    fetchUserDetails();
    fetchUserLists()
  }, []);

  const fetchUserDetails = async () => {

    try {
      const username = getCookie('username')
      const accessToken = getCookie('access_token')
      if (!username && !accessToken) {
        // console.log("User is not logged in")
      }
      else {
        const details = await getUserDetails(username, accessToken);
        if (details.status = 200) {
          if (details.data.success == true) {
            // console.log("const detaiils:")
            // console.log(details.data)
            setUser(details.data.data)
            // console.log(details.data.data)
            // return details
          }
          else {
            console.log(details.data.message)
          }
        }
      }
    } catch (error) {
      console.log(error)
    }
  };

  const fetchUserLists = async () => {
    try {
        const username = getCookie('username');
        const accessToken = getCookie('access_token');
        if (!!username && !!accessToken) {
            const details = await getUserLists(username, accessToken);
            if (details.status = 200) {
                if (details.data.success == true) {
                    // console.log("const detaiils:")
                    console.log(details.data)
                    setLists(details.data.data)
                    console.log(details.data.data)
                    // return details
                }
                else {
                    console.log(details.data.message)
                }
            }
        }
        else {
            console.log("User is not logged in");
            navigate('/')

        }
    } catch (error) {
        console.log(error);
    }
};



  const handleSaveToList = async (listId, eventId) => {
    // console.log(`Saving event ${eventId} to list ${listId}`);
    // console.log(user);
    try {
      const username = getCookie('username')
      const accessToken = getCookie('access_token')
      if (!username && !accessToken) {
        // console.log("User is not logged in")
      }
      else {
        const response = await addEventToList(username, accessToken, listId, eventId);

        if (response.status == 200) {
          if (response.data.success == true) {
            // console.log("Event successfully added to list");
          } else {
            console.log(response.data.message);
          }
        } else {
          console.log(response.data.message);
          console.log(response.data.data);
        }
      }
    } catch (error) {
      console.log(error);
    }

    handleClose();
  };

  //fetch the list of events when the component mounts
  useEffect(() => {
    checkLoginStatus();
  }, []);

  const [loggedIn, setLoggedIn] = useState('');
  const checkLoginStatus = () => {
    try {
      const username = getCookie('username')
      const accessToken = getCookie('access_token')
      if (!username && !accessToken) {
        // console.log("User is not logged in")
        setLoggedIn(false)
      }
      else {
        // console.log("User is logged in")
        setLoggedIn(true)

      }

    } catch (error) {
      console.log(error)
    }

  }



  return (
    <div>


      {loggedIn ? (

        <Modal show={show} onHide={handleClose} centered>
          <Modal.Header className="add-event-modal-header">
            <h1>Add to List</h1>
          </Modal.Header>
          <Modal.Body className="add-event-modal-body">
            <ListGroup className="add-event-modal-list">
              <ListGroup.Item className="add-event-modal-btn-li">
                <CreateListModal  />
              </ListGroup.Item>


              {user && lists && lists.map((list, index) => (
                <ListGroup.Item  className="add-event-modal-li" key={index} onClick={() => handleSaveToList(list.id, eventId)} >
                  {list.name}
                </ListGroup.Item>
              ))}

            </ListGroup>
          </Modal.Body>
          <Modal.Footer className="add-event-modal-footer">
            <Button className="btn-7" onClick={handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>

      ) : (
        <Modal show={show} onHide={handleClose} centered>
          <Modal.Header  className="add-event-modal-header">
            <Modal.Body  className="add-event-modal-body">User Not Logged In</Modal.Body>
          </Modal.Header>

        </Modal>
      )}
    </div>
  );
}

export default AddToList;
