import { useState } from 'react';
import { createCustomUserList } from '../lib/api/backend-server';
import { getCookie } from '../cookieConfig';
import { useNavigate } from 'react-router-dom';
import validator from 'validator';

function CreateList(props) {

     //react Router hook for navigation
  const navigate = useNavigate();
 
    const [formData, setFormData] = useState({
        name: '',
        events: [],
        collaborative: false,
        collaborators: [],
        visibility: false,
    });
    const [error, setError] = useState({});

    const handleChange = (e) => {
        const { name, type, checked } = e.target;

        const inputValue = type === 'checkbox' ? checked : e.target.value;

        setFormData((prevData) => ({
            ...prevData,
            [name]: inputValue,
        }));
    };

    const validateInput = () => {
        let errors = {};
      
        if (!validator.isLength(formData.name, { min: 1, max: 120})) {
          errors.name = 'Name must be between 1-120 characters long';
        }
      
        setError(errors);
        return Object.keys(errors).length === 0;
      };

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!validateInput()) {
            return;
        }
          
        try {
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            if (!!username && !!accessToken) {
                const response = await createCustomUserList(username, accessToken, formData);
                if (response.status = 200) {
                    if (response.data.success == true) {
                        console.log(response.data.message)
                        console.log(response.data.data);

                        //close modal after creating list
                        window.location.reload()
                        
                    }
                }
                else {
                    console.log(response.data.message)
                    setError({error: response.data.message})
                }
            }
        } catch (error) {
            console.error(error);
            const message = error.response.data.error
            navigate('/error', { state: { data: message } });

        }
    };

    return (
        <div className="create-list-body">
            {/* Form for creating a custom user list */}
            <div className="row edit-list-title">
                <h1>Create Custom User List</h1>
            </div>

            <div className="row create-list-form">
                <form onSubmit={handleSubmit}>
                <div className="create-list-input">
                        <label htmlFor="name">List name:</label>
                        <input className="edit-list-textfield" type="text" name="name" value={formData.name} onChange={handleChange} required />
                        {error.username && <span style={{ color: 'red' }}>{error.username}</span>}
                    </div>
                    <div className="create-list-input">
                        <label htmlFor="collaborative">Collaborative:</label>
                        <input type="checkbox" name="collaborative" checked={formData.collaborative} onChange={handleChange} />
                    </div>
                    
                    <div className="create-list-input">
                        <label htmlFor="visibility">Show on Profile:</label>
                        <input className="create-list-checkbox" type="checkbox" name="visibility" checked={formData.visibility} onChange={handleChange} />
                        
                        {error.error && <span style={{ color: 'red' }}>{error.error}</span>}
                    
                    </div>
                    <button className="btn btn-1" type="submit">Create</button>
                </form>
            </div>
        </div>
    );
}

export default CreateList;
