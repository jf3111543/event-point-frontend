import React, { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import CreateList from './CreateList';
import EditList from './EditList';
import AddCollaborator from './AddCollaborator'

function CreateListModal() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      {/* button to open modal containing create custom user list form */}
      <Button className="btn btn-1" onClick={handleShow}>
        Create new list
      </Button>

      <Modal show={show} onHide={handleClose} centered>

        <Modal.Body className="list-modals-modal">
          <CreateList handleClose={handleClose} />
        </Modal.Body>
        <Modal.Footer className="list-modals-modal">
          <Button className="btn btn-7" onClick={handleClose}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

function EditListModal({listId}) {
  // console.log("passed down list id: ", listId)
  const selected_list_id = listId
  // console.log("passing down this: ", selected_list_id)
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      {/* button to open modal containing edit custom user list form */}
      <Button className="btn btn-2" onClick={handleShow}>
        Edit
      </Button>

      <Modal show={show} onHide={handleClose} centered>

        <Modal.Body className="list-modals-modal">
          <EditList handleClose={handleClose} list_id={selected_list_id} />
        </Modal.Body>
        <Modal.Footer className="list-modals-modal">
          <Button className="btn btn-7" onClick={handleClose}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

function AddCollaboratorModal({listId}) {
  // console.log("passed down list id: ", listId)
  const selected_list_id = listId
  // console.log("passing down this: ", selected_list_id)
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      {/* button to open modal containing add collaborator form */}
      <Button className="btn btn-6" onClick={handleShow}>
        Add Collaborator
      </Button>

      <Modal show={show} onHide={handleClose} centered>

        <Modal.Body className="list-modals-modal">
          <AddCollaborator handleClose={handleClose} list_id={selected_list_id} />
        </Modal.Body>
        <Modal.Footer className="list-modals-modal">
          <Button className="btn btn-7" onClick={handleClose}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export {
 CreateListModal,
 EditListModal,
 AddCollaboratorModal
}
