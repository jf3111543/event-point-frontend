import React from 'react';
import { getCookie } from '../cookieConfig';
import { sendFriendRequest } from '../lib/api/backend-server';

function SendFriendRequest({ receiver_username }) {

    const handleClick = async () => {
        try {
            console.log("handle click function called")
            const username = getCookie('username')
            const accessToken = getCookie('access_token')

            console.log(receiver_username)
            if (!!receiver_username) {
                const friendRequestData = { senderUsername: username, receiverUsername: receiver_username }
                const response = await sendFriendRequest(username, accessToken, friendRequestData)
                // console.log("response", response)
                if (response.status == 200) {
                    if (response.data.success == true) {
                        console.log(response.data.data)
                        window.alert(`a friend request was sent to ${receiver_username}`)
                    }
                } else {
                    console.log(response.data.message);
                }
            }


        } catch (error) {
            console.log(error)
        }

    }




    return (
        <div>
            <button className="btn btn-5" onClick={handleClick} >Send Friend Request</button>
        </div>
    );
};

export default SendFriendRequest;