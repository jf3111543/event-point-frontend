import { useEffect, useState } from 'react';
import { getUserDetails } from '../lib/api/backend-server';
import { getCookie } from '../cookieConfig';
import { useNavigate } from 'react-router-dom';
import { addCollaborator } from '../lib/api/backend-server'

function AddCollaboratorToList({ list_id }) {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    //react Router hook for navigation
    const navigate = useNavigate();

    // console.log("selectedListId", list_id)

    //holds the list data
    const [list, setList] = useState([]);
    const [user, setUser] = useState([]);

    //fetch the list of events when the component mounts
    useEffect(() => {
        fetchUserDetails();
    }, []);

    const fetchUserDetails = async () => {
        try {
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            if (!!username && !!accessToken) {
                const details = await getUserDetails(username, accessToken);
                if (details.status = 200) {
                    if (details.data.success == true) {
                        // console.log("const detaiils:")
                        console.log(details.data)
                        setUser(details.data.data)
                        console.log(details.data.data)
                        // return details
                    }
                    else {
                        console.log(details.data.message)
                    }
                }
            }
            else {
                console.log("User is not logged in");
                navigate('/')

            }
        } catch (error) {
            console.log(error);
        }
    };


    const handleAddToList = async (collaboratorUsername) => {
        try {
            console.log("handleAddToList component called")
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            console.log(list_id)
            if (!!username && !!accessToken) {
                const response = await addCollaborator(username, accessToken, list_id, collaboratorUsername);
                if (response.status = 200) {
                    if (response.data.success == true) {
                        // console.log(response.data)
                        // console.log(response.data.message)
                        // console.log(response.data.data);

                        window.location.reload()
                    }
                }
                else {
                    console.log(response.data.message)
                }
            }
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className="add-collaborator-body">
            <div className="row add-collaborator-title">
                <h4>Add a Collaborator to List</h4>
            </div>
            <div className="row">
                <ul>

                    {user.friends && user.friends.length > 0 ? (
                        user.friends.map((friend, index) => (
                            <li className="add-collaborator-li" key={index} onClick={() => handleAddToList(friend.friendUsername)} >
                                {friend.friendUsername}
                            </li>
                        ))
                    ) : (
                        <li>
                            No Friends found
                        </li>
                    )}
                </ul>
            </div>
        </div>
    );
}

export default AddCollaboratorToList;
