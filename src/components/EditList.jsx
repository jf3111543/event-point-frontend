import { useEffect, useState } from 'react';
import { editCustomUserList } from '../lib/api/backend-server';
import { getCookie } from '../cookieConfig';
import { getListDetails } from '../lib/api/backend-server';
import { useNavigate } from 'react-router-dom';
import DeleteList from './DeleteList';
import validator from 'validator';

function EditCustomUserList({ list_id }) {

    //react Router hook for navigation
    const navigate = useNavigate();

    // console.log("selectedListId", list_id)

    //fetch the list of events when the component mounts
    useEffect(() => {
        fetchListDetails();
    }, []);

    const [formData, setFormData] = useState({
        name: '',
        events: [],
        collaborative: false,
        collaborators: [{}],
        visibility: false,
    });
    const [error, setError] = useState({});

    const handleChange = (e) => {
        const { name, type, checked } = e.target;

        const inputValue = type === 'checkbox' ? checked : e.target.value;

        setFormData((prevData) => ({
            ...prevData,
            [name]: inputValue,
        }));
    };

    const validateInput = () => {
        let errors = {};
    
        if (!validator.isLength(formData.name, { min: 1, max: 120})) {
          errors.name = 'Name must be between 1-120 characters long';
        }
        setError(errors);
        return Object.keys(errors).length === 0;
      };

    //get the data of selected list
    const fetchListDetails = async () => {

        try {
            // console.log("fetch list details with this id: ", list_id)
            const username = getCookie('username')
            const accessToken = getCookie('access_token')
            const response = await getListDetails(username, accessToken, list_id);

            if (response.status == 200) {
                if (response.data.success == true) {
                    console.log("List succesfully found");
                    console.log(response.data.data)


                    setFormData({
                        name: response.data.data.name,
                        events: response.data.data.events,
                        collaborative: response.data.data.collaborative,
                        collaborators: response.data.data.collaborators,
                        visibility: response.data.data.visibility
                    })
                }
            } else {
                console.log(response.data.message);
            }
        } catch (error) {
            console.log(error);
        }
    }

    
    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!validateInput()) {
            return;
        }
          
        try {
            console.log("editCustomUserList component called")
            const username = getCookie('username');
            const accessToken = getCookie('access_token');
            // console.log(list_id)
            // console.log(formData)
            if (!!username && !!accessToken) {
                const response = await editCustomUserList(username, accessToken, list_id, formData);
                if (response.status = 200) {
                    if (response.data.success == true) {
                        // console.log(response.data.message)
                        // console.log(response.data.data);
                        window.location.reload()
                    }
                }
                else {
                    console.log(response.data.message)
                    setError({error: response.data.message})
                }
            }
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className="edit-list-body">
            {/* Form for editing a custom user list */}
            <div className="row edit-list-title">
                <h1>Edit Custom User List</h1>
            </div>

            <div className="row edit-list-form">
                <form onSubmit={handleSubmit}>
                    <div className="edit-list-input">
                        <label htmlFor="name">List name:</label>
                        <input className="edit-list-textfield" type="text" name="name" value={formData.name} onChange={handleChange} required />
                        {error.username && <span style={{ color: 'red' }}>{error.username}</span>}
                    </div>
                    <div className="edit-list-input">
                        <label htmlFor="collaborative">Collaborative:</label>
                        <input type="checkbox" name="collaborative" checked={formData.collaborative} onChange={handleChange} />
                    </div>

                    <div className="edit-list-input">
                        <label htmlFor="visibility">Show on Profile:</label>
                        <input className="edit-list-checkbox" type="checkbox" name="visibility" checked={formData.visibility} onChange={handleChange} />

                    </div>
                    <div className="row edit-list-btn-group">
                        <div className="col">
                    <button className="btn btn-1" type="submit">Save</button>
                    </div>
                    <div className="col">
                    <DeleteList listId={list_id} />
                    </div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default EditCustomUserList;
